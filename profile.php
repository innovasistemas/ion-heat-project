<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="src/css/bootstrap.min.css" rel="stylesheet">
        <!-- jQuery-UI CSS -->
        <link href="src/css/jquery-ui-1.12-1.min.css" rel="stylesheet">
        <!-- jQuery datatable CSS -->
        <link href="src/css/jquery.dataTables.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="src/css/mdb.min.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="src/css/style.css" rel="stylesheet">
        <link rel="icon" href="src/static/img/cropped-Ion-Heat_Logo-2-32x32.png" sizes="32x32">
    </head>

    <body onload="verifyAccess(5); loadMainMenu(getPageName()); loadData();">
        
        <!--Menu-->
        <div id="div-header" class="navbar navbar-expand-lg navbar-light bg-light">
            <!--Header-->
                        
            <div id="content-header"></div>
            
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#div-menu" aria-controls="div-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="div-menu"></div>
            
            <!--User session-->
            <ul id="jsddm" class="text-capitalize" style="border-radius: 7px;">
                <li class="margin-div-user">
                    <img src='src/static/img/user-login.png' class="d-inline" />
                    <a href="#" id="link-login" class="text-center d-inline" style="border-radius: 7px;">Management</a>
                    <ul style="border-radius: 7px;">
                        <li>
                            <a href="#" id="link-profile">Profile</a>
                        </li>
                        <li>
                            <a href="#!" id="link-logout">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--end menu-->

        <!--Main container-->
        <div class="container-fluid">
            <!--Grid row-->
            <div class="row">
                
                <!--Grid column-->
                <div class="col-lg-6 mb-4">
                    <!--Card-->
                    <div class="card height-card">
                        <!--Card content-->
                        <div class="card-body" style="margin-left: 20%;">
                            <h2>Profiles</h2>
                            <form>
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtId" class="font-weight-bold">Id</label>
                                        <input name="txtId" id="txtId" class="form-control input-lg w-75" disabled="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtDescription" class="font-weight-bold">Description</label>
                                        <input name="txtDescription" id="txtDescription" class="form-control input-lg w-75" maxlength="255" />
                                    </div>
                                </div>

                                <div class="row">&nbsp;</div>

                                <div class="row">
                                    <div class="col-12">
                                        <input type="button" value="Save" class="btn btn-success btn-md text-capitalize" id="btnSave" title="Save data" />
                                        <input type="reset" value="Cancel" class="btn btn-info btn-md text-capitalize" id="btnReset" title="Cancel" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--/.Card-->
                <!--Grid column-->
                </div>
                
                <!--Grid column-->
                <div class="col-lg-6 mb-4">
                    <!--Card-->
                    <div class="card height-card">
                        <!--Card content-->
                        <div class="card-body">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <label class="font-weight-bold">Profiles</label>
                                        <table id='tableData' class="table hover mdl-data-table">
                                            <!-- <caption><div id='resultTotal'>Recipes data</div></caption> -->
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Id</th>
                                                    <th class="text-center">Description</th>
                                                    <th class="text-center">Order</th>
                                                    <th class="text-center">Active</th>
                                                    <th class="text-center">Creation date</th>
                                                    <th class="text-center">Edit</th>
                                                    <th class="text-center">Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.Card-->
                <!--Grid column-->
                </div>
            <!--Grid row-->
            
            <div id="dialog" title="System information"></div>
            
        </div>
        <!--Main container-->

        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="src/js/jquery-3.4.1.min.js"></script>
        <!-- jQuery-UI -->
        <script type="text/javascript" src="src/js/jquery-ui-1.12.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="src/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
        <!--jQuery data tables-->
        <script type="text/javascript" src="src/js/jquery.dataTables.min.js"></script>
        <!--Bootstrap data tables-->
        <!--<script type="text/javascript" src="src/js/dataTables.bootstrap.min.js"></script>-->
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="src/js/mdb.min.js"></script>
        <script type="text/javascript" src="src/js/plotly.min.js"></script>
        <!--Global function to encrypt-->
        <script type="text/javascript" src="src/js/aes3.1.2.js"></script>
        <!--Global functions Javascript-->
        <script type="text/javascript" src="src/js/scripts-commons.js"></script>
        <script type="text/javascript" src="src/js/functions-commons.js"></script>

        <script>
            
            $(function(){
                setInterval(function(){
                    verifyAccess(5); 
                }, 5000);
                
                
                $("#btnSave").click(function(){
                    if(validataData()){
                        var objJson = {
                            id: $("#txtId").val(),
                            description: $("#txtDescription").val(),
                            selectType: 'save'
                        };

                        $.ajax({
                            url: 'webservices/profile-webservice.php',
                            type: 'POST',
                            dataType: 'json',
                            data: JSON.stringify(objJson),
                            success: function(data){
                                $( "#dialog" ).html(data.message);
                                $( "#dialog" ).dialog({
                                    autoOpen: true,
                                    modal: true,
                                    dialogClass: 'hide-close',      
                                    closeOnEscape: false,
                                    buttons: {
                                        "Ok": function () {
                                            $(this).dialog("close");
                                            location.reload();
                                        }
                                    } 
                                });
                            }
                        }).done(function(){

                        });
                    }    
                });
                
            });
            
            
            function loadData()
            {
                var objJson = {
                    selectType: 'read'
                };
                
                $.ajax({
                    url: 'webservices/profile-webservice.php',
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(objJson),
                    success: function(data){
                        var table = "";
                        $.each(data, function(index, value){
                            table += "<tr>";
                            table += "<td class='text-center'>" + value.id + "</td>";
                            table += "<td class='text-center'>" + value.description + "</td>";
                            table += "<td class='text-center'>" + value.order + "</td>";
                            table += "<td class='text-center'>" + value.active + "</td>";
                            table += "<td class='text-center'>" + value.creation_date + "</td>";
                            table += "<td class='text-center'>";
                            table += "<a href='#!' class='link-update btn-link' title='Click to update' onclick='updateRecord(" + JSON.stringify(value) + ");'>";
                            table += "<img src='src/static/img/edit.png' class='img img-thumbnail' />";
                            table += "</a>";
                            table += "<td class='text-center'>";
                            table += "<a href='#!' class='link-update btn-link' title='Click to delete' onclick='deleteRecord(" + JSON.stringify(value) + ");'>";
                            table += "<img src='src/static/img/delete.png' class='img img-thumbnail' />";
                            table += "</a>";
                            table += "</td>";
                            table += "</tr>";
                        });

                        $('#tableData tbody').html(table);                        
                        $('#tableData').DataTable({
                            "scrollY": "400px",
                            "scrollCollapse": true,
//                            "paging": false,
//                            "info": false,
                            "retrieve": true,
                            language: {
                                "sProcessing":     "Processing...",
                                "sPaginationType": "full_numbers",
                                "sLengthMenu":     "Show _MENU_ records per page",
                                "sZeroRecords":    "No results found",
                                "sEmptyTable":     "No data available in this table",
                                "sInfo":           "Showing records from _START_ to _END_ of a total of _TOTAL_ records",
                                "sInfoEmpty":      "Showing records from 0 al 0 of a total of 0 records",
                                "sInfoFiltered":   "(filtering a total of _MAX_ records)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Search",
                                "sSearchPlaceholder": "Search",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Loading...",
                                "oPaginate": {
                                    "sFirst":    "First",
                                    "sLast":     "Last",
                                    "sNext":     "Next",
                                    "sPrevious": "Previous",
                                    "pagingType":"simple"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activate to sort the column in ascending order",
                                    "sSortDescending": ": Activate to sort the column in descending order"
                                }
                            }
                        });   
                    }
                }).done(function(){

                });
            }
            
            
            //            Delete record
            function deleteRecord(objFields)
            {
                $( "#dialog" ).html("Are you sure to delete record?");
                $( "#dialog" ).dialog({
                    title: "System information",
                    autoOpen: true,
                    modal: true,
                    buttons: [
                        {
                            text: "Ok",
                            click: function(){
                                $(this).dialog("close"); 

                                var objJson = {
                                    id: objFields.id,
                                    selectType: 'delete'
                                };

                                $.ajax({
                                    url: 'webservices/profile-webservice.php',
                                    type: 'POST',
                                    dataType: 'json',
                                    data: JSON.stringify(objJson),
                                    success: function(data){
                                        $( "#dialog" ).html(data.message);
                                        $( "#dialog" ).dialog({
                                            autoOpen: true,
                                            modal: true,
                                            dialogClass: 'hide-close',      
                                            closeOnEscape: false,
                                            buttons: {
                                                "Ok": function () {
                                                    $(this).dialog("close");
                                                    location.reload();
                                                }
                                            } 
                                        });
                                    }
                                }).done(function(){
                                    
                                });
                            }
                        },
                        {
                            text: "Cancel",
                            click: function(){
                               $(this).dialog("close"); 
                            }
                        }
                    ] 
                });
            }
            
            
            //            Update record
            function updateRecord(objFields)
            {
                $( "#dialog" ).html("Are you sure to update record?");
                $( "#dialog" ).dialog({
                    title: "System information",
                    autoOpen: true,
                    modal: true,
                    dialogClass: 'hide-close',
                    closeOnEscape: false,
                    buttons: [
                        {
                            text: "Ok",
                            click: function(){
                                $(this).dialog("close"); 
                                $("#txtId").val(objFields.id);
                                $("#txtDescription").val(objFields.description);
                            }
                        },
                        {
                            text: "Cancel",
                            click: function(){
                                $(this).dialog("close"); 
                            }
                        }
                    ]
                });
            }
            
            
            function validataData()
            {
                $("#txtDescription").val($("#txtDescription").val().trim());
                var description = $("#txtDescription").val();
                var sw = false;
                
                if(description.length >= 3 && description.length <= 255){
                    sw = true;
                }else{
                    showMessage('The description must contain at least 3 characters and a maximum of 255');
                }
                return sw;
            }
            
        </script>
    </body>
</html>



