<?php
/**
 * Web service to validate user's credentials 
 */

include 'connection.php';

// Configure local date
date_default_timezone_set("America/Bogota");

// Receive form data
$json = file_get_contents('php://input');

// Convert to PHP object
$data = json_decode($json, FALSE);

if(!empty($data)){
    switch($data->accessIndex){
        case 1: // Validate and create access 
            // Connect and consult the database
            $query = "SELECT profile.id, description, id_user, user, fullname 
                      FROM profile INNER JOIN users 
                      ON profile.id=users.profile_id 
                      WHERE BINARY user='{$data->username}' AND BINARY password=AES_ENCRYPT('{$data->password}', '$aesKey')";
            $resultQuery = mysqli_query($connect, $query);

            if(mysqli_num_rows($resultQuery) == 1){
                $resultData = mysqli_fetch_array($resultQuery);

                $fullName = $resultData['fullname'];
                $description = $resultData['description'];
                $token = getToken();
                $ipAccess = $_SERVER['REMOTE_ADDR'];
                $userAgent = $_SERVER['HTTP_USER_AGENT'];

                $query = "INSERT INTO access(token, ip_access, user_agent, users_id)
                          VALUES('$token', '$ipAccess', '$userAgent', '{$resultData['id_user']}')
                         ";
                if(mysqli_query($connect, $query)){
                    $arrayResult = [
                        "full_name" => $fullName,
                        "profile" => $description,
                        "message" => "Access allowed",
                        "token" => $token,
                        "ip_access" => $ipAccess,
                        "user_agent" => $userAgent,
                        "access" => mysqli_insert_id($connect),
                        "error" => 0
                    ];
                }else{
                    $arrayResult = [
                        "message" => "Occurred a problem. Session data wasn't save; you don't have permission to access",
                        "error" => 103
                    ];
                }
            }else{
                $arrayResult = [
                    "message" => "Invalid credentials",
                    "error" => 102
                ];
            }
            break;
        case 2: // Verify access
            $query = "SELECT access.*  
                      FROM users INNER JOIN access 
                        ON id_user=users_id 
                      WHERE 
                        user='{$data->username}' AND 
                        token='{$data->token}' AND
                        ip_access='{$data->ip_access}' AND    
                        user_agent='{$data->user_agent}'    
                     ";
            $resultQuery = mysqli_query($connect, $query);

            if(mysqli_num_rows($resultQuery) == 1){
                $arrayResult = [
                    "message" => "Access allowed",
                    "error" => 0
                ];
            }else{
                $arrayResult = [
                    "message" => "You don't permission to access to this page",
                    "error" => 104
                ];
            }
            break;
        case 3: // Update record to close session
            $query = "UPDATE 
                      users INNER JOIN access 
                        ON id_user=users_id
                      SET exit_date='" . date("Y-m-d H:i:s") . "'
                      WHERE 
                        user='{$data->username}' AND 
                        token='{$data->token}' AND
                        ip_access='{$data->ip_access}' AND    
                        user_agent='{$data->user_agent}' 
                     ";
            if(mysqli_query($connect, $query)){
                $arrayResult = [
                    "message" => "You logged out correctly",
                    "error" => 0
                ];
            }else{
                $arrayResult = [
                    "message" => "Occurred a problem. It couldn't logout",
                    "error" => 105
                ];
            }
            break;
        case 4: // Get profile components 
            $query = "SELECT components.id, name, link, main_menu, logo
                      FROM profile INNER JOIN permissions 
                        ON profile.id=profile_id
                      INNER JOIN components
                        ON components.id=components_id
                      WHERE description='{$data->profile}'
                      ORDER BY components.order
                     ";
            $resultQuery = mysqli_query($connect, $query);
            
            if(mysqli_num_rows($resultQuery) > 0){
                while($row = mysqli_fetch_array($resultQuery)){
                    $arrayIdComponents[] = $row['id'];
                    $arrayComponents[] = $row['name'];
                    $arrayLinksComponents[] = $row['link'];
                    $arrayMainMenuComponents[] = $row['main_menu'];
                    $arrayImagesComponents[] = $row['logo'];
                }
                
                $arrayResult = [
                    "idcomponent" => implode(",", $arrayIdComponents),
                    "permissions" => implode(",", $arrayComponents),
                    "links" => implode(",", $arrayLinksComponents),
                    "main_menu" => implode(",", $arrayMainMenuComponents),
                    "logos" => implode(",", $arrayImagesComponents),
                    "message" => "Access allowed",
                    "error" => 0
                ];
            }else{
                $arrayResult = [
                    "message" => "Permission denied",
                    "error" => 106
                ];
            }
            break;
        case 5: // Validate admin access 
            $query = "SELECT access.*  
                      FROM profile 
                      INNER JOIN users ON profile.id=users.profile_id
                      INNER JOIN access ON id_user=users_id 
                      WHERE 
                        user='{$data->username}' AND 
                        description='admin' AND 
                        token='{$data->token}' AND 
                        ip_access='{$data->ip_access}' AND  
                        user_agent='{$data->user_agent}'  
                     ";
            $resultQuery = mysqli_query($connect, $query);

            if(mysqli_num_rows($resultQuery) == 1){
                $arrayResult = [
                    "message" => "Access allowed",
                    "error" => 0
                ];
            }else{
                $arrayResult = [
                    "message" => "You don't permission to access to this page",
                    "error" => 107
                ];
            }
            break;
    }
}else{
    $arrayResult = [
        "message" => "No data",
        "error" => 101
    ];
}

echo json_encode($arrayResult);


// -----------------------------------------------------------------------------
// PHP functions
// -----------------------------------------------------------------------------

// Function to get token
function getToken()
{
    $characters = "0123456789abcdefghijklmnopqrstuvwxyz" . 
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ_@+-*%/$#=&[]{}()~|<>?^.,;:";
    $lenCharacters = strlen($characters);
    $token = "";
    for($i=0; $i < 30; $i++){
        $token .= $characters[rand(0, $lenCharacters - 1)]; 
    }
    return $token;
}


