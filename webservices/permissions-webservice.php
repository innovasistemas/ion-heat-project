<?php
/**
 * Web service to return all furnaces or a furnace particular
 */

include 'connection.php';

// Receive form data
$json = file_get_contents('php://input');

// Convert to PHP object
$data = json_decode($json, FALSE);

if(!empty($data)){
    switch($data->selectType){
        case 'general':
            $query = "SELECT * 
                      FROM permissions 
                    ";
            $resultQuery = mysqli_query($connect, $query);

            // Array data 
            $arrayRecords = [];
            while($row = mysqli_fetch_array($resultQuery)){
                $arrayRecords[] = $row;
            }
            
            // Free memory result     
            mysqli_free_result($resultQuery);
            
            break;
        case 'search':
            $query = "SELECT * 
                      FROM permissions
                      WHERE profile_id = '{$data->profile}'
                     ";
            $resultQuery = mysqli_query($connect, $query);

            // Array data 
            $arrayRecords = [];
            while($row = mysqli_fetch_array($resultQuery)){
                $arrayRecords[] = $row;
            }
            
            // Free memory result     
            mysqli_free_result($resultQuery); 
            
            break;
        case 'insert-delete':
            foreach($data->permissionComponent as $value){
                $query = "SELECT * 
                          FROM permissions
                          WHERE components_id = '$value' AND profile_id = '{$data->profile}'
                         ";
                $resultQuery = mysqli_query($connect, $query);
            
                if(mysqli_num_rows($resultQuery) == 0){
                    // Insert permissions per profile
                    $query = "INSERT INTO permissions(profile_id, components_id)
                              VALUES('{$data->profile}', '$value')
                             ";
                    $resultQuery = mysqli_query($connect, $query);
                    
                    
                    // Insert order cards per user
                    $query = "INSERT INTO users_components(users_id, components_id, users_components.order)
                              SELECT permissions 
                             ";
                }
            }
            
            foreach($data->noPermissionComponent as $value){
                $query = "DELETE 
                          FROM permissions
                          WHERE components_id = '$value' AND profile_id = '{$data->profile}'
                         ";
                $resultQuery = mysqli_query($connect, $query);
            }
            
            $arrayRecords = [
                "message" => "Successful operation",
                "error" => 0
            ];
            
            break;
        default:
            break;
    }
}


// Closed connection
mysqli_close($connect);

// Data encoding in json format
echo json_encode($arrayRecords);



