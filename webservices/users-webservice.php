<?php
/**
 * Web service to return all furnaces or a furnace particular
 */

include 'connection.php';

// Receive form data
$json = file_get_contents('php://input');

// Convert to PHP object
$data = json_decode($json, FALSE);

if(!empty($data)){
    switch($data->selectType){
        case 'read':
            $query = "SELECT profile.description, users.* 
                      FROM profile 
                      RIGHT JOIN users
                      ON profile.id = profile_id
                     ";
            $resultQuery = mysqli_query($connect, $query);
            $arrayRecords = [];
            $i = 0;
            while($row = mysqli_fetch_array($resultQuery)){
                $arrayRecords[$i]['id_user'] = $row['id_user'];
                $arrayRecords[$i]['user'] = utf8_encode($row['user']);
                $arrayRecords[$i]['password'] = utf8_encode($row['password']);
                $arrayRecords[$i]['user_name'] = utf8_encode($row['user_name']);
                $arrayRecords[$i]['user_last_name'] = utf8_encode($row['user_last_name']);
                $arrayRecords[$i]['fullname'] = utf8_encode($row['fullname']);
                $arrayRecords[$i]['user_role'] = utf8_encode($row['user_role']);
                $arrayRecords[$i]['position'] = utf8_encode($row['position']);
                $arrayRecords[$i]['activities'] = utf8_encode($row['activities']);
                $arrayRecords[$i]['active'] = utf8_encode($row['active']);
                $arrayRecords[$i]['profile_id'] = utf8_encode($row['profile_id']);
                $arrayRecords[$i]['description'] = utf8_encode($row['description']);
                $arrayRecords[$i]['creation_date'] = utf8_encode($row['creation_date']);
                $i++;
            }
            mysqli_free_result($resultQuery); 
            
            break;
        case 'search':
            $query = "SELECT * 
                      FROM users 
                      WHERE id = '{$data->id}'
                    ";
                      
            break;
        case 'delete':
            $query = "SELECT id 
                      FROM access 
                      WHERE users_id = '{$data->id}'
                     ";
            $resultQuery = mysqli_query($connect, $query);
            $rowsAccess = mysqli_num_rows($resultQuery);
            
            if($rowsAccess == 0){
                $query = "DELETE 
                          FROM users 
                          WHERE id_user = '{$data->id}' AND user <> 'admin'
                         ";
                if(mysqli_query($connect, $query)){
                    $arrayRecords = [
                        "message" => "Record deleted correctly",
                        "error" => 0
                    ];
                }else{
                    $arrayRecords = [
                        "message" => "A problem ocurred and the record couldn't be deleted",
                        "error" => 101
                    ];
                }
            }else{
                $arrayRecords = [
                    "message" => "You can't delete the user because have records related with others tables or because this record corresponds to the admin",
                    "error" => 100
                ];
            }
            
            break;
        case 'save':
            $firstName = utf8_decode($data->firstName);
            $lastName = utf8_decode($data->lastName);
            $role = utf8_decode($data->role);
            $position = utf8_decode($data->position);
            $activities = utf8_decode($data->activities);
            $fullName = utf8_decode($data->fullName);
            $user = utf8_decode($data->user);
            $password = $data->password;
            $passwordPrevious = $data->passwordPrevious;
            $profile = utf8_decode($data->profile);
            
            if($data->id == ''){
                $query = "INSERT INTO users(user_name, user_last_name, user_role, position, activities, fullname, user, password, profile_id)
                          VALUES
                            ('$firstName', '$lastName', '$role',
                            '$position',  '$activities', '$fullName', 
                            '$user', AES_ENCRYPT('$password', '$aesKey'), '$profile') 
                         ";
            }else{
                if($password != $passwordPrevious){
                    $query = "UPDATE users
                              SET user_name='$firstName', user_last_name='$lastName', user_role='$role',
                                  position='$position', activities='$activities', fullname='$fullName',
                                  user='$user', password=AES_ENCRYPT('$password', '$aesKey'), profile_id='$profile'
                              WHERE id_user = '{$data->id}'
                             ";
                }else{
                    $query = "UPDATE users
                              SET user_name='$firstName', user_last_name='$lastName', user_role='$role',
                                  position='$position', activities='$activities', fullname='$fullName',
                                  user='$user', profile_id='$profile'
                              WHERE id_user = '{$data->id}'
                             ";
                }
            }
            
            if(mysqli_query($connect, $query)){
                $arrayRecords = [
                    "message" => "Record saved correctly",
                    "error" => 0
                ];
            }else{
                $arrayRecords = [
                    "message" => "A problem ocurred and the record couldn't be saved. It's possible that the record already exists",
                    "error" => 201
                ];
            }
            
            break;
        case 'update-password':
            $currentPassword = $data->currentPassword;
            $newPassword = $data->newPassword;
            $user = utf8_decode($data->user);
            
            $query = "SELECT user 
                      FROM users 
                      WHERE BINARY user = '$user' AND BINARY password = AES_ENCRYPT('$currentPassword', '$aesKey')
                     ";
            $resultQuery = mysqli_query($connect, $query);
            
            if(mysqli_num_rows($resultQuery) > 0){
                $query = "UPDATE users
                          SET password = AES_ENCRYPT('$newPassword', '$aesKey')
                          WHERE BINARY user = '$user'
                         ";
                if(mysqli_query($connect, $query)){
                    $arrayRecords = [
                        "message" => "Record updated correctly",
                        "error" => 0
                    ];
                }else{
                    $arrayRecords = [
                        "message" => "A problem ocurred and the password couldn't be updated.",
                        "error" => 301
                    ];
                }
            }else{
                $arrayRecords = [
                    "message" => "The current password not match with the password saved in the database",
                    "error" => 300
                ];
            }
            
            break;
        default:
            break;
    }
}


// Closed connection
mysqli_close($connect);

// Data encoding in json format
echo json_encode($arrayRecords, JSON_UNESCAPED_UNICODE);



