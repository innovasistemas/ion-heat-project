<?php
/**
 * Web service to return all furnaces or a furnace particular
 */

include 'connection.php';

// Receive form data
$json = file_get_contents('php://input');

// Convert to PHP object
$data = json_decode($json, FALSE);

if(!empty($data)){
    switch($data->selectType){
        case 'read':
            $query = "SELECT * 
                      FROM access 
                    ";
            $resultQuery = mysqli_query($connect, $query);

            // Array data 
            $arrayRecords = [];
            while($row = mysqli_fetch_array($resultQuery)){
                $arrayRecords[] = $row;
            }
            
            // Free memory result     
            mysqli_free_result($resultQuery);
            
            break;
        case 'search':
            $query = "SELECT access.*, user 
                      FROM users 
                      INNER JOIN access
                      ON id_user = users_id
                      WHERE id_user = '{$data->id}'
                      ORDER BY id DESC
                    ";

            $resultQuery = mysqli_query($connect, $query);

            // Array data 
            $arrayRecords = [];
            while($row = mysqli_fetch_array($resultQuery)){
                $arrayRecords[] = $row;
            }

            // Free memory result     
            mysqli_free_result($resultQuery);
                      
            break;
        default:
            break;
    }
}

// Closed connection
mysqli_close($connect);

// Data encoding in json format
echo json_encode($arrayRecords, JSON_UNESCAPED_UNICODE);



