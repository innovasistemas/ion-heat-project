<?php
/**
 * Web service to return all furnaces or a furnace particular
 */

include 'connection.php';

// Receive form data
$json = file_get_contents('php://input');

// Convert to PHP object
$data = json_decode($json, FALSE);

if(!empty($data)){
    switch($data->selectType){
        case 'read':
            $query = "SELECT * 
                      FROM profile
                     ";
            $resultQuery = mysqli_query($connect, $query);

            $arrayRecords = [];
            while($row = mysqli_fetch_array($resultQuery)){
                $arrayRecords[] = $row;
            }

            mysqli_free_result($resultQuery); 
            
            break;
        case 'search':
            $query = "SELECT * 
                      FROM profile 
                      WHERE id = '{$data->id}'
                    ";
                      
            break;
        case 'delete':
            $query = "SELECT user 
                      FROM users 
                      WHERE profile_id = '{$data->id}'
                     ";
            $resultQuery = mysqli_query($connect, $query);
            $rowsUsers = mysqli_num_rows($resultQuery);
            
            $query = "SELECT id 
                      FROM permissions 
                      WHERE profile_id = '{$data->id}'
                     ";
            $resultQuery = mysqli_query($connect, $query);
            $rowsPermissions = mysqli_num_rows($resultQuery);
            
            if($rowsUsers == 0 && $rowsPermissions == 0){
                $query = "DELETE 
                          FROM profile 
                          WHERE id = '{$data->id}' AND description <> 'admin'
                         ";
                if(mysqli_query($connect, $query)){
                    $arrayRecords = [
                        "message" => "Record deleted correctly",
                        "error" => 0
                    ];
                }else{
                    $arrayRecords = [
                        "message" => "A problem ocurred and the record couldn't be deleted",
                        "error" => 101
                    ];
                }
            }else{
                $arrayRecords = [
                    "message" => "You can't delete the profile because have records related with others tables",
                    "error" => 100
                ];
            }
            
            break;
        case 'save':
            if($data->id == ''){
                $query = "INSERT INTO profile(description)
                          VALUES('{$data->description}') 
                          ";
            }else{
                $query = "UPDATE profile
                          SET description='{$data->description}'
                          WHERE id = '{$data->id}'
                         ";
            }
            
            if(mysqli_query($connect, $query)){
                $arrayRecords = [
                    "message" => "Record saved correctly",
                    "error" => 0
                ];
            }else{
                $arrayRecords = [
                    "message" => "A problem ocurred and the record couldn't be saved. It's possible that the record already exists",
                    "error" => 200
                ];
            }
            
            break;
        default:
            break;
    }
}


// Closed connection
mysqli_close($connect);

// Data encoding in json format
echo json_encode($arrayRecords);



