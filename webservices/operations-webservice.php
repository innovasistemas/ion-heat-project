<?php
/**
 * Web service to return all furnaces or a furnace particular
 */

include 'connection.php';

// Receive form data
$json = file_get_contents('php://input');

// Convert to PHP object
$data = json_decode($json, FALSE);

if(!empty($data)){
    switch($data->selectType){
        case 'read':
            $query = "SELECT * 
                      FROM operations 
                    ";
            $resultQuery = mysqli_query($connect, $query);

            // Array data 
            $arrayRecords = [];
            while($row = mysqli_fetch_array($resultQuery)){
                $arrayRecords[] = $row;
            }
            
            // Free memory result     
            mysqli_free_result($resultQuery);
            
            break;
        case 'search':
            $query = "SELECT operations.*, name 
                      FROM access 
                      INNER JOIN operations
                        ON access.id = access_id
                      INNER JOIN components
                        ON components.id = components_id
                      WHERE access.id = '{$data->id}'
                    ";
                      
            $resultQuery = mysqli_query($connect, $query);

            // Array data 
            $arrayRecords = [];
            while($row = mysqli_fetch_array($resultQuery)){
                $arrayRecords[] = $row;
            }
            
            // Free memory result     
            mysqli_free_result($resultQuery);
                      
            break;
        case 'save':
            $query = "INSERT INTO operations(description, access_id, components_id)
                      VALUES('{$data->description}','{$data->accessId}','{$data->componentId}') 
                     ";
            
            if(mysqli_query($connect, $query)){
                $arrayRecords = [
                    "message" => "Record saved correctly",
                    "error" => 0
                ];
            }else{
                $arrayRecords = [
                    "message" => "A problem ocurred and the record couldn't be saved. It's possible that the record already exists",
                    "error" => 200
                ];
            }
            
            break;
        default:
            break;
    }
}


// Closed connection
mysqli_close($connect);

// Data encoding in json format
echo json_encode($arrayRecords, JSON_UNESCAPED_UNICODE);



