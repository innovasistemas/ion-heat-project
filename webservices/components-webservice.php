<?php
/**
 * Web service to return all furnaces or a furnace particular
 */

include 'connection.php';

// Receive form data

//$objData = json_decode($_POST['dataSend'], TRUE); // for arrays
$objData = json_decode($_POST['dataSend'], FALSE); // for objects
$data = new stdClass();

if(!empty($objData->fields)){
    $data->id = $objData->fields->id;
    $data->name = $objData->fields->name;
    $data->link = $objData->fields->link;
    $data->order = $objData->fields->order;
    $data->mainMenu = $objData->fields->mainMenu;
    $data->selectType = $objData->fields->selectType;
}

if(!empty($data)){
    switch($data->selectType){
        case 'read':
            $query = "SELECT * 
                      FROM components
                     ";
            $resultQuery = mysqli_query($connect, $query);
            $arrayRecords = [];
            while($row = mysqli_fetch_array($resultQuery)){
                $arrayRecords[] = $row;
            }
            mysqli_free_result($resultQuery); 
            
            break;
        case 'search':
            $query = "SELECT * 
                      FROM components 
                      WHERE id = '{$data->id}'
                    ";
                      
            break;
        case 'delete':
            $query = "SELECT id 
                      FROM permissions 
                      WHERE profile_id = '{$data->id}'
                     ";
            $resultQuery = mysqli_query($connect, $query);
            $rowsPermissions = mysqli_num_rows($resultQuery);
            
            if($rowsPermissions == 0){
                $arrayFile = deleteFile($connect, $data->id);
                $query = "DELETE 
                          FROM components 
                          WHERE id = '{$data->id}'
                         ";
                if(mysqli_query($connect, $query)){
                    $arrayRecords = [
                        "message" => "Record deleted correctly",
                        "error" => 0
                    ];
                }else{
                    $arrayRecords = [
                        "message" => "A problem ocurred and the record couldn't be deleted",
                        "error" => 101
                    ];
                }
            }else{
                $arrayRecords = [
                    "message" => "You can't delete the component because have records related with others tables",
                    "error" => 100
                ];
            }
            
            break;
        case 'save':
            if($data->id == ''){
                if(!empty($_FILES)){
                    $arrayFile = loadFile();
                    $query = "INSERT INTO components(name, link, components.order, main_menu, logo)
                              VALUES('{$data->name}', '{$data->link}', '{$data->order}', '{$data->mainMenu}', '{$arrayFile['nameFile']}') 
                             ";
                }else{
                    $query = "INSERT INTO components(name, link, components.order, main_menu)
                              VALUES('{$data->name}', '{$data->link}', '{$data->order}', '{$data->mainMenu}') 
                             ";
                }
            }else{
                if(!empty($_FILES)){
                    $arrayFile = deleteFile($connect, $data->id);
                    $arrayFile = loadFile();
                    $query = "UPDATE components
                              SET name='{$data->name}', link='{$data->link}', components.order='{$data->order}', main_menu='{$data->mainMenu}', logo='{$arrayFile['nameFile']}'
                              WHERE id = '{$data->id}'
                             ";
                }else{
                    $query = "UPDATE components
                              SET name='{$data->name}', link='{$data->link}', components.order='{$data->order}', main_menu='{$data->mainMenu}'
                              WHERE id = '{$data->id}'
                             ";
                }
            }
            
            if(mysqli_query($connect, $query)){
                $arrayRecords = [
                    "message" => "Record saved correctly",
                    "error" => 0
                ];
            }else{
                $arrayRecords = [
                    "message" => "A problem ocurred and the record couldn't be saved. It's possible that the record already exists",
                    "error" => 200
                ];
            }
            
            break;
        case 'search-users-components':
            $query = "SELECT users_components.* 
                      FROM users
                      INNER JOIN users_components 
                        ON id_user=users_id
                      INNER JOIN components
                        ON components.id=components_id
                      WHERE user = '{$data->user}'
                     ";
            $resultQuery = mysqli_query($connect, $query);
            $rowsUsersComponents = mysqli_num_rows($resultQuery);
            break;
        default:
            break;
    }
}


// Closed connection
mysqli_close($connect);

// Data encoding in json format
echo json_encode($arrayRecords);


// Function to load files in the server (used to images)
function loadFile()
{ 
    $responseFile = "";
    $nameFile = "thumbnail.png";
    $errorFile = 0;

    // Check if there is an image to upload
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && 
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 
            'xmlhttprequest'){
        if(!empty($_FILES['inputFile'])){
            $fName = $_FILES['inputFile']['name'];
            $fName = changeFileName($fName, '');

            $fType = $_FILES['inputFile']['type'];
            $fSize = $_FILES['inputFile']['size'];
            $fTempName = $_FILES['inputFile']['tmp_name'];
            
            if((strpos($fType, "jpeg") || strpos($fType, "png") || 
                    strpos($fType, "gif")) && $fSize <= 3145728){
                if(move_uploaded_file($fTempName, 
                        "../src/images/images-components/" . $fName)){
                    $nameFile = $fName;
                    $responseFile = "Uploaded file correctly";
                }else{
                    $responseFile = "Occurred an error to uploaded file";
                    $errorFile = 301;
                }
            }else{
                $responseFile = "Invalid file type or size";
                $errorFile = 302;
            }
        }else{
            $responseFile = "Unspecified file";
            $errorFile = 303;
        }
    }else{
        $responseFile = "Error processing request uploaded file";
        $errorFile = 304;
    }

    $arrayFile = [
        "responseFile" => $responseFile, 
        "nameFile" => $nameFile, 
        "errorFile" => $errorFile
    ];

    return $arrayFile;
}


// Function to rename files
function changeFileName($nameFile, $folder)
{
    $lengthNameFile = strlen($nameFile);
    $lastPointPosition = strrpos($nameFile, '.');
    $name = substr($nameFile, 0, $lastPointPosition);

    $name = $name . "-" . time();
    $ext = substr($nameFile, $lastPointPosition + 1, $lengthNameFile - 
        ($lastPointPosition + 1));
    $nameFile = $name . "." . $ext;
    return $nameFile;
}


// Function to delete file correspondent to the record deleted
function deleteFile($connect,$id)
{
    $responseFile = "";
    $errorFile = 0;
    
    $query = "SELECT * 
              FROM components 
              WHERE id = '$id'
             ";
    $resultQuery = mysqli_query($connect, $query);
    $arrayResult = mysqli_fetch_assoc($resultQuery);
    
    $route = "../src/images/images-components/" . $arrayResult['logo'];

    if(file_exists($route) && $arrayResult['logo'] != 'thumbnail.png'){
        if(unlink($route)){
            $responseFile = "File removed correctly";
        }else{
            $responseFile = "You can't remove file; ocurred a problem with the file";
            $errorFile = 305;
        }
    }else{
        $responseFile = "You can't file remove. It is possible don't exists";
        $errorFile = 306;
    }


    $arrayFile = [
        "responseFile" => $responseFile, 
        "nameFile" => $arrayResult['logo'], 
        "errorFile" => $errorFile
    ];

    return $arrayFile;
}
