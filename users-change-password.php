<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="src/css/bootstrap.min.css" rel="stylesheet">
        <!-- jQuery-UI CSS -->
        <link href="src/css/jquery-ui-1.12-1.min.css" rel="stylesheet">
        <!-- jQuery datatable CSS -->
        <link href="src/css/jquery.dataTables.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="src/css/mdb.min.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="src/css/style.css" rel="stylesheet">
        <link rel="icon" href="src/static/img/cropped-Ion-Heat_Logo-2-32x32.png" sizes="32x32">
    </head>

    <body onload="verifyAccess(2); loadMainMenu(getPageName()); loadData();">
        
        <!--Menu-->
        <div id="div-header" class="navbar navbar-expand-lg navbar-light bg-light">
            <!--Header-->
                        
            <div id="content-header"></div>
            
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#div-menu" aria-controls="div-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="div-menu"></div>
            
            <!--User session-->
            <ul id="jsddm" class="text-capitalize" style="border-radius: 7px;">
                <li class="margin-div-user">
                    <img src='src/static/img/user-login.png' class="d-inline" />
                    <a href="#" id="link-login" class="text-center d-inline" style="border-radius: 7px;">Management</a>
                    <ul style="border-radius: 7px;">
                        <li>
                            <a href="#" id="link-profile">Profile</a>
                        </li>
                        <li>
                            <a href="#!" id="link-logout">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--end menu-->

        <!--Main container-->
        <div class="container-fluid">
            <!--Grid row-->
            <div class="row"></div>

            <!--Grid row-->
            <div class="row align-items-center">
                
                <!--Grid column-->
                <div class="col-lg-12 mb-4">
                    <!--Card-->
                    <div class="card">
                        <!--Card content-->
                        <div class="card-body" style="margin-left: 28%;">
                            <h2>Change password</h2>
                            <form name="frmRegister" id="frmRegister">
                                <div class="row">
                                    <div class="col-12">
                                        <label for="txtUserName" class="control-label font-weight-bold" id="lblUsername">Username</label>
                                        <input type="text" id="txtUserName" name="txtUserName" placeholder="Username" required="" class="form-control input-md w-50" maxlength="100" disabled="" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <label for="txtCurrentPassword" class="control-label font-weight-bold" id="lblCurrentPassword">Current password</label>
                                        <input type="password" id="txtCurrentPassword" name="txtCurrentPassword" placeholder="Current password" required="" class="form-control input-md w-50" maxlength="100" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <label for="txtNewPassword" class="control-label font-weight-bold" id="lblNewPassword">New password</label>
                                        <input type="password" id="txtNewPassword" name="txtNewPassword" placeholder="New password" required="" class="form-control input-md w-50" maxlength="100" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <label for="txtConfirmPassword" class="control-label font-weight-bold" id="lblConfirmPassword">Confirm password</label>
                                        <input type="password" id="txtConfirmPassword" name="txtConfirmPassword" placeholder="Confirm password" required="" class="form-control input-md w-50" maxlength="100" />
                                    </div>
                                </div>
                                <div class="row">&nbsp;</div>
                                <div class="col-md-12">
                                    <div class="form-group text-bottom">
                                        <input type="button" value="Save" class="btn btn-primary btn-md text-capitalize" id="btnSave" title="Save data" />
                                        <input type="button" value="Cancel" class="btn btn-info btn-md text-capitalize" id="btnReset" title="Cancel" />
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                        
                    </div>
                    <!--/.Card-->
                </div>
                <!--Grid column-->
                
            </div>
            <!--Grid row-->
            
            <div id="dialog" title="System information"></div>
            
        </div>
        <!--Main container-->

        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="src/js/jquery-3.4.1.min.js"></script>
        <!-- jQuery-UI -->
        <script type="text/javascript" src="src/js/jquery-ui-1.12.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="src/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
        <!--jQuery data tables-->
        <script type="text/javascript" src="src/js/jquery.dataTables.min.js"></script>
        <!--Bootstrap data tables-->
        <!--<script type="text/javascript" src="src/js/dataTables.bootstrap.min.js"></script>-->
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="src/js/mdb.min.js"></script>
        <script type="text/javascript" src="src/js/plotly.min.js"></script>
        <!--Global function to encrypt-->
        <script type="text/javascript" src="src/js/aes3.1.2.js"></script>
        <!--Global functions Javascript-->
        <script type="text/javascript" src="src/js/scripts-commons.js"></script>
        <script type="text/javascript" src="src/js/functions-commons.js"></script>

        <script>
            
            $(function(){
                setInterval(function(){
                    verifyAccess(2); 
                }, 5000);
                
                
                $("#btnSave").click(function(){
                    if(validataData()){
                        var objJson = {
                            user: $("#txtUserName").val(),
                            currentPassword: $("#txtCurrentPassword").val(),
                            newPassword: $("#txtNewPassword").val(),
                            selectType: 'update-password'
                        };
                        
                        var strJson = JSON.stringify(objJson);
                        
                        $.ajax({
                            url: 'webservices/users-webservice.php',
                            type: 'POST',
                            dataType: 'json',
                            data: strJson,
                            success: function(data){
                                $( "#dialog" ).html(data.message);
                                $( "#dialog" ).dialog({
                                    autoOpen: true,
                                    modal: true,
                                    dialogClass: 'hide-close',      
                                    closeOnEscape: false,
                                    buttons: {
                                        "Ok": function () {
                                            $(this).dialog("close");
                                            location.reload();
                                        }
                                    } 
                                });
                            }
                        }).done(function(){

                        });
                    }    
                });
                
                
                $("#btnReset").click(function(){
                    $("#txtCurrentPassword").val('');
                    $("#txtNewPassword").val('');
                    $("#txtConfirmPassword").val('');
                });
                
                
//                $("#txtCurrentPassword").keydown(function(event){
//                    return validateQuotes(this, event.keyCode)
//                });
//                
//                
//                $("#txtNewPassword").keydown(function(event){
//                    return validateQuotes(event.keyCode)
//                });
//                
//                
//                $("#txtConfirmPassword").keydown(function(event){
//                    return validateQuotes(event.keyCode)
//                });
                
            });
            
            
            function loadData()
            {
                var userEncrypt = localStorage.getItem(btoa("username"));
                var user = CryptoJS.AES.decrypt(userEncrypt, key).toString(CryptoJS.enc.Utf8);
                $("#txtUserName").val(user);
            }
            
            
            function validataData()
            {
                var userName = $("#txtUserName").val().trim();
                var currentPassword = $("#txtCurrentPassword").val().trim();
                var newPassword = $("#txtNewPassword").val().trim();
                var confirmPassword = $("#txtConfirmPassword").val().trim();
                var sw = false;
                if(userName.length > 2 && userName.length <= 255 && userName !== "" && userName.indexOf(" ", 0) === -1){
                    if(currentPassword.length > 2 && currentPassword.length <= 100 && currentPassword !== ""){
                        if(newPassword.length > 2 && newPassword.length <= 100 && newPassword !== ""){
                            if(newPassword === confirmPassword){
                                sw = true;
                            }else{
                                showMessage('Passwords do not match');
                            }
                        }else{
                            showMessage('Invalid new password. Enter a password with a minimum length of three characters and a maximum of one hundred');
                        }
                    }else{
                        showMessage('Invalid current password. Enter a password with a minimum length of three characters and a maximum of one hundred');
                    }
                }else{
                    showMessage('The user field must contain at least 3 characters and a maximum of 255 without white spaces');
                }
                return sw;
            }
            
        </script>
    </body>
</html>



