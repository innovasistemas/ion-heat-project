      //================================================================================
      //////////////////////////////////////////////////////////////////////////////////

      var geocoder = new google.maps.Geocoder();

      var map; 

      var marker;

      //================================================================================
      //////////////////////////////////////////////////////////////////////////////////
      function geocodePosition(pos) {
        geocoder.geocode({
          latLng: pos
        }, function(responses) {
          if (responses && responses.length > 0) {
            updateMarkerAddress(responses[0].formatted_address);



          } else {
            updateMarkerAddress('Cannot determine address at this location.');
          }
        });

      }

      //================================================================================
      //////////////////////////////////////////////////////////////////////////////////
      function updateMarkerStatus(str) {
        //document.getElementById('markerStatus').innerHTML = str;
      }

      //================================================================================
      //////////////////////////////////////////////////////////////////////////////////
      function updateMarkerPosition(latLng) {
        //document.getElementById('info').innerHTML = [
        //  latLng.lat(),
        //  latLng.lng()
        //].join(', ');
        document.getElementById("georef").value = [
          latLng.lat(),
          latLng.lng()
        ].join(', ');
      }

      //================================================================================
      //////////////////////////////////////////////////////////////////////////////////
      function updateMarkerAddress(str) {
        //document.getElementById('address').innerHTML = str;
        var temp = str.split(",");
        //console.log(str);
        
        if (temp[0]=="Unnamed Road"){
          temp[0] = "";
        }
       text1=forceUnicodeEncoding(temp[0]);
        document.getElementById('corregimiento').value = text1;
        document.getElementById('municipio').value = temp[1];




      }

      //================================================================================
      //////////////////////////////////////////////////////////////////////////////////
      function changeMarkerPosition(latLng) {
        marker.setPosition(latLng);
        console.log(latLng);
        document.getElementById("georef").value = [
          latLng.lat(),
          latLng.lng()
        ].join(', ');
     




      }

      //================================================================================
      //////////////////////////////////////////////////////////////////////////////////
      //Update the Google map for the user's inputted address
      function UpdateMap( )
      {

        // Get the user's inputted address
        var address = document.getElementById("addressInput").value;

      
        // Make asynchronous call to Google geocoding API
        geocoder.geocode( { 'address': address }, function(results, status) {
          var addr_type = results[0].types[0];  // type of address inputted that was geocoded
          if ( status == google.maps.GeocoderStatus.OK ) 
            ShowLocation( results[0].geometry.location, address, addr_type );

           
          else     
            alert("Geocode was not successful for the following reason:" + status);        
        });
      }      

      //================================================================================
      //////////////////////////////////////////////////////////////////////////////////
      //Show the location (address) on the map.
      function ShowLocation( latLng, address, addr_type )
      {
        // Center the map at the specified location
        map.setCenter( latLng );


      
        // Set the zoom level according to the address level of detail the user specified
        var zoom = 12;
        switch ( addr_type )
        {
        case "administrative_area_level_1"  : zoom = 6; break;    // user specified a state
        case "locality"                     : zoom = 10; break;   // user specified a city/town
        case "street_address"               : zoom = 15; break;   // user specified a street address
        }
        map.setZoom( zoom );
        
        // Place a Google Marker at the same location as the map center 
        // When you hover over the marker, it will display the title
        changeMarkerPosition(latLng);
        geocodePosition(marker.getPosition()); 


        
        // Create an InfoWindow for the marker
        var contentString = "" + address + "";  // HTML text to display in the InfoWindow
        var infowindow = new google.maps.InfoWindow( { content: contentString } );
        
        // Set event to display the InfoWindow anchored to the marker when the marker is clicked.
        google.maps.event.addListener( marker, 'click', function() { infowindow.open( map, marker ); });
      }

      //================================================================================
      //////////////////////////////////////////////////////////////////////////////////
      //
      function initialize() {
        // Create a Google coordinate object for where to initially center the map
        var latLng = new google.maps.LatLng(8.759126737271016, -75.92110682148439); // Monteria

        var mapOptions = { zoom: 8, center: latLng, mapTypeId: google.maps.MapTypeId.ROADMAP }
        map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);

        marker = new google.maps.Marker({
          position: latLng,
          title: 'Point A',
          map: map,
          draggable: true
        });

        // Update current position info.
        updateMarkerPosition(latLng);
        geocodePosition(latLng);

        // Add dragging event listeners.
        google.maps.event.addListener(marker, 'dragstart', function() {
          updateMarkerAddress('Dragging...');
        });

        google.maps.event.addListener(marker, 'drag', function() {
          updateMarkerStatus('Dragging...');
          updateMarkerPosition(marker.getPosition());
        });

        google.maps.event.addListener(marker, 'dragend', function() {
          updateMarkerStatus('Drag ended');
          geocodePosition(marker.getPosition());
        });
      }


      // Onload handler to fire off the app.
      google.maps.event.addDomListener(window, 'load', initialize);

      


function forceUnicodeEncoding(string) {
  return unescape(encodeURIComponent(string));
}
      