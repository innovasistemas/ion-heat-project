// Constants and variables global

const urlWebService = 'http://localhost/webservices/'

var key = localStorage.getItem(btoa('key'));


// Allows you to change the image visually as files are selected
const readURL = (input) => {
 
    if (input.files && input.files[0]) {
        const reader = new FileReader();

        reader.onload = (e) => {
            $('#logoViewUpdate').attr('src', e.target.result);
            $("#logoViewUpdate").attr('class', 'show img-thumbnail');
        }
        reader.readAsDataURL(input.files[0]);
    }
};


function validateFile(input, maxSize)
{
    var sw = false;
    if(typeof(input.files[0]) !== 'undefined'){
        var file = input.files[0];
        var fName = file.name;
        var fExtension = fName.substring(fName.lastIndexOf('.') + 1);
        var fSize = file.size;

        if(/gif|jpeg|jpg|png$/i.test(fExtension)){
            if(fSize <= maxSize){
                sw = true;
            }else{
                showMessage('File size invalid. The maximum size allowed is 3MB');
            }
        }else{
            showMessage('File type incorrect. Select only jpg, png or gif files');
        }
    }else{
        showMessage('You must specify an image file')
    }
    return sw;
}


function getPageName()
{
    var path = window.location.pathname;
    var page = path.substring(path.lastIndexOf('/') + 1);
    return page;
}


function showMessage(message = "", HeaderTitle = "System information")      
{   
    $( "#dialog" ).html(message);
    $( "#dialog" ).dialog({
        title: HeaderTitle,
        autoOpen: true,
        modal: true,
        dialogClass: 'hide-close',
        closeOnEscape: false,
        buttons: {
            "Ok": function(){
                $(this).dialog("close"); 
            }
        } 
    });
}


function verifyAccess(accessIndex)
{
    if(localStorage.length > 0 && localStorage.getItem(btoa("username")) !== null){
        var userEncrypt = localStorage.getItem(btoa("username"));
        var user = CryptoJS.AES.decrypt(userEncrypt, key).toString(CryptoJS.enc.Utf8);
        
        var objJson = {
            'username': user,
            'token': localStorage.getItem('token'),
            'ip_access': localStorage.getItem('ip_access'),
            'user_agent': localStorage.getItem('user_agent'),
            'accessIndex': accessIndex
        };

        $.ajax({
            url: 'webservices/validate-credentials-webservice.php',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(objJson),
            success: function(data) {
                if(data.error !== 0){
                    $( "#dialog" ).html("You don't permission to access to this page");
                    $( "#dialog" ).dialog({
                        autoOpen: true,
                        modal: true,
                        dialogClass: 'hide-close',
                        closeOnEscape: false,
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close"); 
                                if(accessIndex !== 5){
                                    localStorage.clear();
                                    $(location).attr('href', 'index.php');
                                }else{
                                    $(location).attr('href', 'home.php');
                                }
                            }
                        } 
                    });
                }
            }
        }).done(function(){

        }).fail(function(jqXHR, textStatus, errorThrown){
            if (jqXHR.status === 0){
                showMessage("You can't access the webservice at this moment because it isn't available or there isn't connection. Please try later or contact the administrator");
            }else if(jqXHR.status === 404){
                showMessage('Requested page not found [404]');
            }else if(jqXHR.status === 500){
                showMessage('Internal Server Error [500]');
            }else if(textStatus === 'parsererror'){
                showMessage('Requested JSON parse failed');
            }else if(textStatus === 'timeout'){
                showMessage('Time out error');
            }else if(textStatus === 'abort'){
                showMessage('Ajax request aborted');
            }else{
                showMessage('Uncaught Error: ' + jqXHR.responseText);
            }
        });
    }else{
        $( "#dialog" ).html("You don't permission to access to this page");
        $( "#dialog" ).dialog({
            autoOpen: true,
            modal: true,
            dialogClass: 'hide-close',
            closeOnEscape: false,
            buttons: {
                "Ok": function(){
                    $(this).dialog("close");  
                    $(location).attr('href', 'index.php');
                }
            } 
        });
    }
}


function closeSession()
{
    $( "#dialog" ).html("Are you sure to log out?");
    $( "#dialog" ).dialog({
        autoOpen: true,
        modal: true,
        dialogClass: 'hide-close',
        closeOnEscape: false,
        buttons: {
            "Ok": function(){
                $(this).dialog("close");
                var userEncrypt = localStorage.getItem(btoa("username"));
                var user = CryptoJS.AES.decrypt(userEncrypt, key).toString(CryptoJS.enc.Utf8);
                
                var objJson = {
                    'username': user,
                    'token': localStorage.getItem('token'),
                    'ip_access': localStorage.getItem('ip_access'),
                    'user_agent': localStorage.getItem('user_agent'),
                    'accessIndex': 3
                };

                $.ajax({
                    url: 'webservices/validate-credentials-webservice.php',
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(objJson),
                    success: function(data) {
                        if(data.error !== 0){
                            showMessage(data.message);
                        }
                        localStorage.clear();
                        $(location).attr('href', 'index.php');
                    }
                }).done(function(){

                }).fail(function(jqXHR, textStatus, errorThrown){
//                        https://cybmeta.com/manejo-de-errores-en-jquery-ajax
                    if (jqXHR.status === 0){
                        showMessage("You can't access the webservice at this moment because it isn't available or there isn't connection. Please try later or contact the administrator");
                    }else if(jqXHR.status === 404){
                        showMessage('Requested page not found [404]');
                    }else if(jqXHR.status === 500){
                        showMessage('Internal Server Error [500]');
                    }else if(textStatus === 'parsererror'){
                        showMessage('Requested JSON parse failed');
                    }else if(textStatus === 'timeout'){
                        showMessage('Time out error');
                    }else if(textStatus === 'abort'){
                        showMessage('Ajax request aborted');
                    }else{
                        showMessage('Uncaught Error: ' + jqXHR.responseText);
                    }
                });
            },
            "Cancel": function(){
                $(this).dialog("close");                            
            }
        } 
    });
}


function validateQuotes(input, characterAscii)
{   
    if(characterAscii === 34 || characterAscii === 219){
        if($(input).val().indexOf("\"", 0) > -1){
            var i = 0;
            while(i < $(input).val().length){
                if($(input).val().charAt(i) === '"'){
                    return false;
                }
                i++;
            }
        }
    }
}


function loadMainMenu(page)
{
    var menuNamesEncrypt = localStorage.getItem(btoa("menu"));
    var strMenuNames = CryptoJS.AES.decrypt(menuNamesEncrypt, key).toString(CryptoJS.enc.Utf8);
    var menuMainMenuEncrypt = localStorage.getItem(btoa("main_menu"));
    var strMainMenu = CryptoJS.AES.decrypt(menuMainMenuEncrypt, key).toString(CryptoJS.enc.Utf8);
    var menuLinksEncrypt = localStorage.getItem(btoa("links"));
    var strMenuLinks = CryptoJS.AES.decrypt(menuLinksEncrypt, key).toString(CryptoJS.enc.Utf8);

    var arrayNamesMenu = strMenuNames.split(',');
    var arrayMainMenu = strMainMenu.split(',');
    var arrayLinksMenu = strMenuLinks.split(',');

    var content = "<ul class='nav navbar-nav mr-auto'>";
    $.each(arrayNamesMenu, function(index, value){
        if(arrayMainMenu[index] === "1"){
            if(arrayLinksMenu[index] === page){
                content += '<li class="nav-item"><a href="' + arrayLinksMenu[index] + '" class="font-weight-bold">';
            }else{
                content += '<li class="nav-item"><a href="' + arrayLinksMenu[index] + '" class="">';
            }
            content += "<span class='text-dark'>" + value + "</span>";
            content += '</a></li>';
        }
    });
    
    content += '<hr style="" id="separator-menu">';
    content += '<li class="nav-item"><a href="#!" class="text-dark link-profile">';
    content += "<span class='text-dark'>Profile</span>";
    content += '</a></li>';
    content += '<li class="nav-item"><a href="#!" class="text-dark link-logout">';
    content += "<span class='text-dark'>Logout</span>";
    content += '</a></li>';
    content += "</ul>";
    $('#div-menu').html(content);
}

// Function to paint the datetime in application
//function timeRequest()
//{
//    var objJson ={
//        'Update_Time': JSON.stringify(true)
//    };
//    $.ajax({
//        url: urlWebService,
//        type: 'POST',
//        dataType: 'json',
//        data: objJson,
//        success: function(data) {
//            $("#element-time").text(data.DATE + " - " + data.TIME);
//        }
//    }).done(function(){
//
//    });
//}


// Function to add an element to the array end
//function addElementArray(arrayData, datum)
//{
//    var position = findElementArray(arrayData, datum);
//    if(position === -1){
//        arrayData.push(datum);
//        return true;
//    }else{
//        return false;
//    }
//}


// Function to find an element in an array
function findElementArray(arrayData, datum)
{
    var pos = -1;
    var i = 0;
    while(i < arrayData.length && pos === -1){
        if(arrayData[i] === datum){
            pos = i;
        }else{
            i++;
        }
    }
    return pos;
}


// Function to validate only numbers in the input numeric data
//function validateOnlyNumbers(e)
//{
//    var key = window.Event ? e.which : e.keyCode
//    return (key >= 48 && key <= 57)
//}


// Function to validate only float numbers in the input numeric data
//function filterFloat(evt, input)
//{
//    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
//    var key = window.Event ? evt.which : evt.keyCode;    
//    var chark = String.fromCharCode(key);
//    var tempValue = input.value + chark;
//    if(key >= 48 && key <= 57){
//        if(!filter(tempValue)){
//            return false;
//        }else{       
//            return true;
//        }
//    }else{
//        if(key === 8 || key === 13 || key === 0) {     
//            return true;              
//        }else if(key === 46){
//            if(!filter(tempValue)){
//                return false;
//            }else{       
//                return true;
//            }
//        }else{
//            return false;
//        }
//    }
//}


//function filter(__val__)
//{
//    var preg = /^([0-9]+\.?[0-9]{0,2})$/; 
//    if(preg.test(__val__)){
//        return true;
//    }else{
//       return false;
//    }
//}


// Configurations for the dropdown menu

var timeout    = 500;
var closetimer = 0;
var ddmenuitem = 0;

function jsddm_open()
{  
    jsddm_canceltimer();
    jsddm_close();
    ddmenuitem = $(this).find('ul').css('visibility', 'visible');
}


function jsddm_close()
{  
    if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');
}


function jsddm_timer()
{  
    closetimer = window.setTimeout(jsddm_close, timeout);
}


function jsddm_canceltimer()
{  
    if(closetimer){  
        window.clearTimeout(closetimer);
        closetimer = null;
    }
}


document.onclick = jsddm_close;
// End confgurations for the dropdown menu




