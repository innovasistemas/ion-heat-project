/* 
 * Scripts commons for the application
 */


$(document).ready(function(){  
    // Configurations for the dropdown menu
    $('#jsddm > li').bind('mouseover', jsddm_open);
    $('#jsddm > li').bind('mouseout',  jsddm_timer);
    
    var contador = 1;
    $('.menu_bar').click(function(){
        if(contador === 1){
            $('#div-menu').animate({
                left: '0'
            });
            contador = 0;
        } else {
            contador = 1;
            $('#div-menu').animate({
                left: '-100%'
            });
        }
    });
});


$('#div-header').delegate('#link-logout', 'click', function(){
    closeSession();
});


$('#div-header').delegate('.link-logout', 'click', function(){
    closeSession();
});


$("#link-profile").click(function(){
    window.location = 'users-change-password.php';
});


$('#div-header').delegate(".link-profile", "click", function(){
    window.location = 'users-change-password.php';
});


$('#content-header').delegate('.link-home', 'click', function(){
    $(location).attr('href', 'home.php');
});


$("#content-header").load('internal-modules/header.php');


$("#link-login").html(localStorage.getItem('full_name'));


$("title").text("IONHEAT DMI TEST");