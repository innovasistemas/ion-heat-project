DELETE FROM users_components

ALTER TABLE users_components AUTO_INCREMENT=1

SELECT COUNT(*) 
FROM users
WHERE profile_id = 1

SELECT COUNT(*) 
FROM components


INSERT INTO users_components(users_id, components_id, users_components.order)
SELECT DISTINCT id_user, permissions.components_id, components.order
FROM users, components INNER JOIN permissions ON components.id=permissions.components_id 
WHERE users.profile_id=1 AND permissions.profile_id=users.profile_id;


DELETE users_components
FROM users INNER JOIN users_components ON (users.id_user=users_components.users_id)
	INNER JOIN components ON (components.id=users_components.components_id)
	INNER JOIN permissions ON components.id=permissions.components_id 
WHERE users.profile_id=1 AND permissions.profile_id=users.profile_id;




SELECT *
FROM permissions
WHERE profile_id='1' 