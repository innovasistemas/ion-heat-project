<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="src/css/bootstrap.min.css" rel="stylesheet">
        <!-- jQuery-UI CSS -->
        <link href="src/css/jquery-ui-1.12-1.min.css" rel="stylesheet">
        <!-- jQuery datatable CSS -->
        <link href="src/css/jquery.dataTables.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="src/css/mdb.min.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="src/css/style.css" rel="stylesheet">
        <link rel="icon" href="src/static/img/cropped-Ion-Heat_Logo-2-32x32.png" sizes="32x32">
    </head>

    <body onload="verifyAccess(5); loadMainMenu(getPageName()); loadData();">
        
        <!--Menu-->
        <div id="div-header" class="navbar navbar-expand-lg navbar-light bg-light">
            <!--Header-->
                        
            <div id="content-header"></div>
            
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#div-menu" aria-controls="div-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="div-menu"></div>
            
            <!--User session-->
            <ul id="jsddm" class="text-capitalize" style="border-radius: 7px;">
                <li class="margin-div-user">
                    <img src='src/static/img/user-login.png' class="d-inline" />
                    <a href="#" id="link-login" class="text-center d-inline" style="border-radius: 7px;">Management</a>
                    <ul style="border-radius: 7px;">
                        <li>
                            <a href="#" id="link-profile">Profile</a>
                        </li>
                        <li>
                            <a href="#!" id="link-logout">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--end menu-->


        <!--Main container-->
        <div class="container-fluid">
            <!--Grid row-->
            <div class="row">
                
                <!--Grid column-->
                <div class="col-lg-5 mb-4">
                    <!--Card-->
                    <div class="card height-card">
                        <!--Card content-->
                        <div class="card-body" style="margin-left: 20%;">
                            <h2>Components</h2>
                            <form name="frmRegister" id="frmRegister" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtId" class="font-weight-bold">Id</label>
                                        <input name="txtId" id="txtId" class="form-control input-lg w-75" disabled="" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtName" class="font-weight-bold">Name</label>
                                        <input name="txtName" id="txtName" class="form-control input-lg w-75" maxlength="255" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtLink" class="font-weight-bold">Link</label>
                                        <input name="txtLink" id="txtLink" class="form-control input-lg w-75" maxlength="255" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtOrder" class="font-weight-bold">Order</label>
                                        <input name="txtOrder" id="txtOrder" type="number" class="form-control input-lg w-75" min="1" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-12 d-inline form-group radio"> 
                                        <label for="optMainMenu" class="font-weight-bold">Main menu</label>
                                        <br>
                                        <input name="optMainMenu" id="optMainMenuYes" type="radio" class="" value="1" />Yes
                                        <input name="optMainMenu" id="optMainMenuNo" type="radio" class="" checked="" value="0" />No
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="fleLogo" class="font-weight-bold">Logo</label>
                                        <input name="fleLogo" id="fleLogo" type="file" class="form-control input-lg w-75" />
                                    </div>
                                    <div class="row">&nbsp;</div>
                                    <div class="text-left" id="divLogoView" style="margin-top: 10px;">
                                        <img id="logoViewUpdate" class="img-thumbnail d-none" alt="" src="#" style="width: 25%;" />
                                        <input type="hidden" name="txtLogoView" id="txtLogoView" value="" />
                                    </div>
                                </div>
                                
                                <div class="row">&nbsp;</div>
                                
                                <div class="row">
                                    <div class="col-12">
                                        <input type="button" value="Save" class="btn btn-success btn-md text-capitalize" id="btnSave" title="Save data" />
                                        <input type="reset" value="Cancel" class="btn btn-info btn-md text-capitalize" id="btnReset" title="Cancel" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--/.Card-->
                </div>
                <!--Grid column-->
                
                <!--Grid column-->
                <div class="col-lg-7 mb-4">
                    <!--Card-->
                    <div class="card height-card">
                        <!--Card content-->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <label class="font-weight-bold">Components</label>
                                    <table id='tableData' class="table hover mdl-data-table">
                                        <!-- <caption><div id='resultTotal'>Recipes data</div></caption> -->
                                        <thead>
                                            <tr>
                                                <th class="text-center">Id</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Link</th>
                                                <th class="text-center">Order</th>
                                                <th class="text-center">Active</th>
                                                <th class="text-center">Main menu</th>
                                                <th class="text-center">Logo</th>
                                                <th class="text-center">Creation date</th>
                                                <th class="text-center">Edit</th>
                                                <th class="text-center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.Card-->
                </div>
                <!--Grid column-->
                
            </div>
            <!--Grid row-->
            
            <div id="dialog" title="System information"></div>
            
        </div>
        <!--Main container-->

        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="src/js/jquery-3.4.1.min.js"></script>
        <!-- jQuery-UI -->
        <script type="text/javascript" src="src/js/jquery-ui-1.12.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="src/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
        <!--jQuery data tables-->
        <script type="text/javascript" src="src/js/jquery.dataTables.min.js"></script>
        <!--Bootstrap data tables-->
        <!--<script type="text/javascript" src="src/js/dataTables.bootstrap.min.js"></script>-->
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="src/js/mdb.min.js"></script>
        <script type="text/javascript" src="src/js/plotly.min.js"></script>
        <!--Global function to encrypt-->
        <script type="text/javascript" src="src/js/aes3.1.2.js"></script>
        <!--Global functions Javascript-->
        <script type="text/javascript" src="src/js/scripts-commons.js"></script>
        <script type="text/javascript" src="src/js/functions-commons.js"></script>

        <script>
            
            $(function(){
                setInterval(function(){
                    verifyAccess(5); 
                }, 5000);
                
                
                $("#btnSave").click(function(){
                    if(validataData()){
                        $('#fleLogo').focus();
                        var isFile = false;
                        if($("#txtLogoView").val().trim().length === 0){
                            isFile = true;
                        }else{
                            if($("#txtLogoView").val() !== $("#logoViewUpdate").attr('src')){
                                isFile = true;
                            }
                        }
                        if(isFile){
                            isFile = validateFile($("#fleLogo")[0], 3145728);
                        }else{
                            isFile = true;
                        }
                        
                        var fields = {
                            id: $("#txtId").val(),
                            name: $("#txtName").val(),
                            link: $("#txtLink").val(),
                            order: $("#txtOrder").val(),
                            mainMenu: $("#optMainMenuYes").is(':checked') ? $("#optMainMenuYes").val() : $("#optMainMenuNo").val(),
                            selectType: 'save'
                        };

                        var objJson = {
                            fields: fields
                        };

                        var strJson = JSON.stringify(objJson);

                        var formData = new FormData();
                        formData.append('dataSend', strJson);

                        if($('form').find('input:file').prop('type') === 'file'){
                            formData.append('inputFile', $('form').find('input:file')[0].files[0]);
                        }
                        
                        if(isFile){
                            $.ajax({
                                url: 'webservices/components-webservice.php',
                                type: 'POST',
                                dataType: 'json',
                                data: formData,
                                contentType: false,
                                processData: false,
                                success: function(data){
                                    $( "#dialog" ).html(data.message);
                                    $( "#dialog" ).dialog({
                                        autoOpen: true,
                                        modal: true,
                                        dialogClass: 'hide-close',      
                                        closeOnEscape: false,
                                        buttons: {
                                            "Ok": function () {
                                                $(this).dialog("close");
                                                location.reload();
                                            }
                                        } 
                                    });
                                }
                            }).done(function(){

                            });
                        }
                    }    
                });
                
                
                $("#fleLogo").change(function(){
                    readURL(this);
                });
                
                
                $("#btnReset").click(function(){
                    $("#logoViewUpdate").attr('src', '');
                });
                
            });
            
            
            function loadData()
            {
                var fields = {
                    id: '',
                    name: '',
                    link: '',
                    order: '',
                    mainMenu: '',
                    selectType: 'read'
                };

                var objJson = {
                    fields: fields
                };
                
                var strJson = JSON.stringify(objJson);
                        
                var formData = new FormData();
                formData.append('dataSend', strJson);
                
                $.ajax({
                    url: 'webservices/components-webservice.php',
                    type: 'POST',
                    dataType: 'json',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(data){
                        var table = "";
                        $.each(data, function(index, value){
                            table += "<tr>";
                            table += "<td class='text-center'>" + value.id + "</td>";
                            table += "<td class='text-center'>" + value.name + "</td>";
                            table += "<td class='text-center'>" + value.link + "</td>";
                            table += "<td class='text-center'>" + value.order + "</td>";
                            table += "<td class='text-center'>" + value.active + "</td>";
                            table += "<td class='text-center'>" + value.main_menu + "</td>";
                            table += "<td class='text-center'>";
                            table += "<img src='src/images/images-components/" + value.logo + "' class='img img-thumbnail' style='width: 30%;' />";
                            table += "</td>";
                            table += "<td class='text-center'>" + value.creation_date + "</td>";
                            table += "<td class='text-center'>";
                            table += "<a href='#!' class='link-update btn-link' title='Click to update' onclick='updateRecord(" + JSON.stringify(value) + ");'>";
                            table += "<img src='src/static/img/edit.png' class='img img-thumbnail' />";
                            table += "</a>";
                            table += "<td class='text-center'>";
                            table += "<a href='#!' class='link-update btn-link' title='Click to delete' onclick='deleteRecord(" + JSON.stringify(value) + ");'>";
                            table += "<img src='src/static/img/delete.png' class='img img-thumbnail' />";
                            table += "</a>";
                            table += "</td>";
                            table += "</tr>";
                        });

                        $('#tableData tbody').html(table);                        
                        $('#tableData').DataTable({
                            "scrollY": "450px",
                            "scrollCollapse": true,
//                            "paging": false,
//                            "info": false,
                            "retrieve": true,
                            language: {
                                "sProcessing":     "Processing...",
                                "sPaginationType": "full_numbers",
                                "sLengthMenu":     "Show _MENU_ records per page",
                                "sZeroRecords":    "No results found",
                                "sEmptyTable":     "No data available in this table",
                                "sInfo":           "Showing records from _START_ to _END_ of a total of _TOTAL_ records",
                                "sInfoEmpty":      "Showing records from 0 al 0 of a total of 0 records",
                                "sInfoFiltered":   "(filtering a total of _MAX_ records)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Search",
                                "sSearchPlaceholder": "Search",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Loading...",
                                "oPaginate": {
                                    "sFirst":    "First",
                                    "sLast":     "Last",
                                    "sNext":     "Next",
                                    "sPrevious": "Previous",
                                    "pagingType":"simple"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activate to sort the column in ascending order",
                                    "sSortDescending": ": Activate to sort the column in descending order"
                                }
                            }
                        });   
                    }
                }).done(function(){

                });
            }
            
            
            //            Delete record
            function deleteRecord(objFields)
            {
                $( "#dialog" ).html("Are you sure to delete record?");
                $( "#dialog" ).dialog({
                    title: "System information",
                    autoOpen: true,
                    modal: true,
                    buttons: [
                        {
                            text: "Ok",
                            click: function(){
                                $(this).dialog("close"); 

                                var objJson = {
                                    id: objFields.id,
                                    selectType: 'delete'
                                };
                                
                                var fields = {
                                    id: objFields.id,
                                    name: '',
                                    link: '',
                                    order: '',
                                    mainMenu: '',
                                    selectType: 'delete'
                                };

                                objJson = {
                                    fields: fields
                                };

                                var strJson = JSON.stringify(objJson);
                                var formData = new FormData();

                                formData.append('dataSend', strJson);

                                $.ajax({
                                    url: 'webservices/components-webservice.php',
                                    type: 'POST',
                                    dataType: 'json',
                                    data: formData,
                                    contentType: false,
                                    processData: false,
                                    success: function(data){
                                        $( "#dialog" ).html(data.message);
                                        $( "#dialog" ).dialog({
                                            autoOpen: true,
                                            modal: true,
                                            dialogClass: 'hide-close',      
                                            closeOnEscape: false,
                                            buttons: {
                                                "Ok": function () {
                                                    $(this).dialog("close");
                                                    location.reload();
                                                }
                                            } 
                                        });
                                    }
                                }).done(function(){
                                    
                                });
                            }
                        },
                        {
                            text: "Cancel",
                            click: function(){
                               $(this).dialog("close"); 
                            }
                        }
                    ] 
                });
            }
            
            
            //            Update record
            function updateRecord(objFields)
            {
                $( "#dialog" ).html("Are you sure to update record?");
                $( "#dialog" ).dialog({
                    title: "System information",
                    autoOpen: true,
                    modal: true,
                    dialogClass: 'hide-close',
                    closeOnEscape: false,
                    buttons: [
                        {
                            text: "Ok",
                            click: function(){
                                $(this).dialog("close"); 
                                $("#txtId").val(objFields.id);
                                $("#txtName").val(objFields.name);
                                $("#txtLink").val(objFields.link);
                                $("#txtOrder").val(objFields.order);
                                parseInt(objFields.main_menu) === 1 ? $("#optMainMenuYes").attr('checked', 'checked') : $("#optMainMenuNo").attr('checked', 'checked'); 
                                if(objFields.logo.trim().length > 0){
                                    $("#logoViewUpdate").attr('class', 'd-inline img-thumbnail');
                                    $("#logoViewUpdate").attr('src', 'src/images/images-components/' + objFields.logo);
                                    $("#txtLogoView").val('src/images/images-components/' + objFields.logo);
                                }
                            }
                        },
                        {
                            text: "Cancel",
                            click: function(){
                                $(this).dialog("close"); 
                            }
                        }
                    ]
                });
            }
            
            
            function validataData()
            {
                $("#txtName").val($("#txtName").val().trim());
                $("#txtLink").val($("#txtLink").val().trim());
                var name = $("#txtName").val();
                var link = $("#txtLink").val();
                var order = $("#txtOrder").val();
                var sw = false;
                
                if(name.length >= 3 && name.length <= 255){
                    if(link.length >= 3 && link.length <= 255){
                        if(order >= 0){
                            sw = true;
                        }else{
                            showMessage('The order field must be greater than 0');
                        }
                    }else{
                        showMessage('The link field must contain at least 3 characters and a maximum of 255');
                    }
                }else{
                    showMessage('The name field must contain at least 3 characters and a maximum of 255');
                }
                return sw;
            }
            
        </script>
    </body>
</html>



