<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">-->
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="src/css/bootstrap.min.css" rel="stylesheet">
        <!-- jQuery-UI CSS -->
        <link href="src/css/jquery-ui-1.12-1.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="src/css/mdb.min.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="src/css/style.css" rel="stylesheet">
        <link rel="icon" href="src/static/img/cropped-Ion-Heat_Logo-2-32x32.png" sizes="32x32">
    </head>

    <body onload="verifyAccess(2); loadMainMenu(getPageName()); loadData();">

        <!--Menu-->
        <div id="div-header" class="navbar navbar-expand-lg navbar-light bg-light">
            <!--Header-->
                        
            <div id="content-header"></div>
            
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#div-menu" aria-controls="div-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="div-menu"></div>
            
            <!--User session-->
            <ul id="jsddm" class="text-capitalize" style="border-radius: 7px;">
                <li class="margin-div-user">
                    <img src='src/static/img/user-login.png' class="d-inline" />
                    <a href="#" id="link-login" class="text-center d-inline" style="border-radius: 7px;">Management</a>
                    <ul style="border-radius: 7px;">
                        <li>
                            <a href="#" id="link-profile">Profile</a>
                        </li>
                        <li>
                            <a href="#!" id="link-logout">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--end menu-->
        
        <!--Main container-->
        <div class="container-fluid">
            <!--Grid row-->
            <div class="row align-items-center" id="div-card-menu"></div>
            
            <div id="dialog" title="System information"></div>
            
        </div>
        <!--Main container-->

        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="src/js/jquery-3.4.1.min.js"></script>
        <!-- jQuery-UI -->
        <script type="text/javascript" src="src/js/jquery-ui-1.12.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="src/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
        <!--jQuery data tables-->
        <script type="text/javascript" src="src/js/jquery.dataTables.min.js"></script>
        <!--Bootstrap data tables-->
        <script type="text/javascript" src="src/js/dataTables.bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="src/js/mdb.min.js"></script>
        <script type="text/javascript" src="src/js/plotly.min.js"></script>
        <!--Global function to encrypt-->
        <script type="text/javascript" src="src/js/aes3.1.2.js"></script>
        <!--Global functions Javascript-->
        <script type="text/javascript" src="src/js/scripts-commons.js"></script>
        <script type="text/javascript" src="src/js/functions-commons.js"></script>

        <script>

            $(function(){
                setInterval(function(){
                    verifyAccess(2); 
                }, 5000);
                
            });
            
            
            function loadData()
            {
                var menuIdEncrypt = localStorage.getItem(btoa("id_component"));
                var strMenuId = CryptoJS.AES.decrypt(menuIdEncrypt, key).toString(CryptoJS.enc.Utf8);
                var menuNamesEncrypt = localStorage.getItem(btoa("menu"));
                var strMenuNames = CryptoJS.AES.decrypt(menuNamesEncrypt, key).toString(CryptoJS.enc.Utf8);
                var menuMainMenuEncrypt = localStorage.getItem(btoa("main_menu"));
                var strMainMenu = CryptoJS.AES.decrypt(menuMainMenuEncrypt, key).toString(CryptoJS.enc.Utf8);
                var menuLinksEncrypt = localStorage.getItem(btoa("links"));
                var strMenuLinks = CryptoJS.AES.decrypt(menuLinksEncrypt, key).toString(CryptoJS.enc.Utf8);
                var menuImagesEncrypt = localStorage.getItem(btoa("logos"));
                var strMenuLogos = CryptoJS.AES.decrypt(menuImagesEncrypt, key).toString(CryptoJS.enc.Utf8);
                
                var arrayIdMenu = strMenuId.split(',');
                var arrayNamesMenu = strMenuNames.split(',');
                var arrayMainMenu = strMainMenu.split(',');
                var arrayLinksMenu = strMenuLinks.split(',');
                var arrayImagesMenu = strMenuLogos.split(',');
                
                var objParams = {};
                var content = "";
                content += '<div class="container">'; 
                content += '<div class="row justify-content-center">'; 
                $.each(arrayNamesMenu, function(index, value){
                    if(arrayMainMenu[index] === "0"){
                        objParams = {
                            id: arrayIdMenu[index],
                            name: arrayNamesMenu[index],
                            link: arrayLinksMenu[index]
                        };
                        content += '<div class="col-lg-2 col-md-6 mb-4 d-flex">';
                        content += '<div class="card">';
                        content += '<div class="card-header" style="height: 80px;">';
                        content += "<a href='#!' class='' onclick='saveOperation(" + JSON.stringify(objParams) + ");'>";
                        content += '<p class="text-dark text-capitalize card-text font-weight-bold">' + value + '</p>';
                        content += '</a>';
                        content += '</div>';
                        content += '<div class="card-body view overlay">';
                        content += "<a href='#!' class='' onclick='saveOperation(" + JSON.stringify(objParams) + ");'>";
                        content += '<img src="src/images/images-components/' + arrayImagesMenu[index] + '" class="card-img-top img-fluid" />';
                        content += '<div class="mask rgba-orange-light waves-effect waves-light"></div>'
                        content += '</a>';
                        content += '</div>';
                        content += '</div>';
                        content += '</div>';
                    }
                });
                content += '</div>';
                content += '</div>';
                $('#div-card-menu').html(content);
            }
            
            
            function saveOperation(objParams)
            {
                var accessEncrypt = localStorage.getItem(btoa("access"));
                var strAccess = CryptoJS.AES.decrypt(accessEncrypt, key).toString(CryptoJS.enc.Utf8);
                
                var objJson = {
                    description: 'Access to: ' + objParams.name,
                    accessId: parseInt(strAccess),
                    componentId: objParams.id,
                    selectType: 'save'
                };

                $.ajax({
                    url: 'webservices/operations-webservice.php',
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(objJson),
                    success: function(data){
                        if(data.error !== 0){
                            $( "#dialog" ).html(data.message);
                            $( "#dialog" ).dialog({
                                autoOpen: true,
                                modal: true,
                                dialogClass: 'hide-close',      
                                closeOnEscape: false,
                                buttons: {
                                    "Ok": function () {
                                        $(this).dialog("close");
                                    }
                                } 
                            });
                        }
                    }
                }).done(function(){
                    $(location).attr('href', objParams.link);
                });
            }
            
        </script>
    </body>
</html>



