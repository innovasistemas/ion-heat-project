<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="src/css/bootstrap.min.css" rel="stylesheet">
        <!-- jQuery-UI CSS -->
        <link href="src/css/jquery-ui-1.12-1.min.css" rel="stylesheet">
        <!-- jQuery datatable CSS -->
        <link href="src/css/jquery.dataTables.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="src/css/mdb.min.css" rel="stylesheet">
        <!--Select2 plugin-->
        <link href="src/css/select2.min.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="src/css/style.css" rel="stylesheet">
        <link rel="icon" href="src/static/img/cropped-Ion-Heat_Logo-2-32x32.png" sizes="32x32">
    </head>

    <body onload="verifyAccess(5); loadMainMenu(getPageName()); loadData();">
        
        <!--Menu-->
        <div id="div-header" class="navbar navbar-expand-lg navbar-light bg-light">
            <!--Header-->
                        
            <div id="content-header"></div>
            
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#div-menu" aria-controls="div-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="div-menu"></div>
            
            <!--User session-->
            <ul id="jsddm" class="text-capitalize" style="border-radius: 7px;">
                <li class="margin-div-user">
                    <img src='src/static/img/user-login.png' class="d-inline" />
                    <a href="#" id="link-login" class="text-center d-inline" style="border-radius: 7px;">Management</a>
                    <ul style="border-radius: 7px;">
                        <li>
                            <a href="#" id="link-profile">Profile</a>
                        </li>
                        <li>
                            <a href="#!" id="link-logout">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--end menu-->

        <!--Main container-->
        <div class="container-fluid">
            <!--Grid row-->
            <div class="row"></div>

            <!--Grid row-->
            <div class="row align-items-center">
                
                <!--Grid column-->
                <div class="col-lg-4 mb-4">
                    <!--Card-->
                    <div class="card height-card">
                        <!--Card content-->
                        <div class="card-body" style="margin-left: 20%;">
                            <h2>Users</h2>
                            <form>
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtId" class="font-weight-bold">Id</label>
                                        <input name="txtId" id="txtId" class="form-control input-lg w-75" disabled="" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtUser" class="font-weight-bold">User</label>
                                        <input name="txtUser" id="txtUser" class="form-control input-lg w-75" maxlength="255" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtPassword" class="font-weight-bold">Password</label>
                                        <input name="txtPassword" id="txtPassword" type="password" class="form-control input-lg w-75" maxlength="100" />
                                        <input name="txtPasswordPrevious" id="txtPasswordPrevious" type="hidden" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtFirstName" class="font-weight-bold">First name</label>
                                        <input name="txtFirstName" id="txtFirstName" class="form-control input-lg w-75" maxlength="100" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtLastName" class="font-weight-bold">Last name</label>
                                        <input name="txtLastName" id="txtLastName" class="form-control input-lg w-75" maxlength="100" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtRole" class="font-weight-bold">Role</label>
                                        <input name="txtRole" id="txtRole" class="form-control input-lg w-75" maxlength="50" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtPosition" class="font-weight-bold">Position</label>
                                        <input name="txtPosition" id="txtPosition" class="form-control input-lg w-75" maxlength="100" />
                                    </div>
                                </div>
                                
                                <div class="row d-none">
                                    <div class="col-12"> 
                                        <label for="txtActivities" class="font-weight-bold">Activities</label>
                                        <input name="txtActivities" id="txtActivities" class="form-control input-lg w-75" maxlength="100" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="cboProfiles" class="font-weight-bold">Profile</label>
                                        <br>
                                        <select name="cboProfiles" id="cboProfiles" class="form-control input-lg w-75 select2"></select>
                                    </div>
                                </div>
                                
                                <div class="row">&nbsp;</div>
                                
                                <div class="row">
                                    <div class="col-12">
                                        <input type="button" value="Save" class="btn btn-success btn-md text-capitalize" id="btnSave" title="Save data" />
                                        <input type="reset" value="Cancel" class="btn btn-info btn-md text-capitalize" id="btnReset" title="Cancel" />
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                        
                    </div>
                    <!--/.Card-->
                </div>
                <!--Grid column-->
                
                <!--Grid column-->
                <div class="col-lg-8 mb-4">
                    <!--Card-->
                    <div class="card height-card">
                        <!--Card content-->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <label class="font-weight-bold">Users</label>
                                    <table id='tableData' class="table hover mdl-data-table">
                                        <!-- <caption><div id='resultTotal'>Recipes data</div></caption> -->
                                        <thead>
                                            <tr>
                                                <th class="text-center">Id</th>
                                                <th class="text-center">User</th>
                                                <th class="text-center">First name</th>
                                                <th class="text-center">Last name</th>
                                                <th class="text-center">Full name</th>
                                                <th class="text-center">Role</th>
                                                <th class="text-center">Position</th>
                                                <th class="text-center d-none">Activities</th>
                                                <th class="text-center">Active</th>
                                                <th class="text-center">Profile</th>
                                                <th class="text-center">Creation date</th>
                                                <th class="text-center">Edit</th>
                                                <th class="text-center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    <!--/.Card-->
                </div>
                <!--Grid column-->
                
            </div>
            <!--Grid row-->
            
            <div id="dialog" title="System information"></div>
            
        </div>
        <!--Main container-->

        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="src/js/jquery-3.4.1.min.js"></script>
        <!-- jQuery-UI -->
        <script type="text/javascript" src="src/js/jquery-ui-1.12.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="src/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
        <!--jQuery data tables-->
        <script type="text/javascript" src="src/js/jquery.dataTables.min.js"></script>
        <!--Bootstrap data tables-->
        <!--<script type="text/javascript" src="src/js/dataTables.bootstrap.min.js"></script>-->
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="src/js/mdb.min.js"></script>
        <script type="text/javascript" src="src/js/plotly.min.js"></script>
        <!--Global function to encrypt-->
        <script type="text/javascript" src="src/js/aes3.1.2.js"></script>
        <!--Select2 plugin-->
        <script type="text/javascript" src="src/js/select2.full.min.js"></script>
        <!--Global functions Javascript-->
        <script type="text/javascript" src="src/js/scripts-commons.js"></script>
        <script type="text/javascript" src="src/js/functions-commons.js"></script>

        <script>
            
            $(function(){
                setInterval(function(){
                    verifyAccess(5); 
                }, 5000);
                
                
                $('#cboProfiles').select2({
                    placeholder: 'Select an option',
                    language: "en",
                    allowClear: true,
                    tags: false,
                });
                
                
                $("#btnSave").click(function(){
                    if(validataData()){
                        var objJson = {
                            id: $("#txtId").val(),
                            firstName: $("#txtFirstName").val(),
                            lastName: $("#txtLastName").val(),
                            role: $("#txtRole").val(),
                            position: $("#txtPosition").val(),
                            activities: $("#txtActivities").val(),
                            fullName: $("#txtFirstName").val() + " " + $("#txtLastName").val(),
                            user: $("#txtUser").val(),
                            password: $("#txtPassword").val(),
                            passwordPrevious: $("#txtPasswordPrevious").val(),
                            profile: $("#cboProfiles").val(),
                            selectType: 'save'
                        };
                        
                        $.ajax({
                            url: 'webservices/users-webservice.php',
                            type: 'POST',
                            dataType: 'json',
                            data: JSON.stringify(objJson),
                            success: function(data){
                                $( "#dialog" ).html(data.message);
                                $( "#dialog" ).dialog({
                                    autoOpen: true,
                                    modal: true,
                                    dialogClass: 'hide-close',      
                                    closeOnEscape: false,
                                    buttons: {
                                        "Ok": function () {
                                            $(this).dialog("close");
                                            $("#txtPasswordPrevious").val('');
                                            location.reload();
                                        }
                                    } 
                                });
                            }
                        }).done(function(){

                        });
                    }    
                });
                
            });
            
            
            function loadData()
            {
                var objJson = {
                    selectType: 'read'
                };
                
                $.ajax({
                    url: 'webservices/profile-webservice.php',
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(objJson),
                    success: function(data){
                        var option = "<option></option>";
                        $.each(data, function(index, value){
                            option += "<option value='" + value.id + "'>" + value.description + "</option>";
                        });
                        $("#cboProfiles").html(option);
                    }
                }).done(function(){
                    
                });
                
                $.ajax({
                    url: 'webservices/users-webservice.php',
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(objJson),
                    success: function(data){
                        var table = "";
                        $.each(data, function(index, value){
                            table += "<tr>";
                            table += "<td class='text-center'>" + value.id_user + "</td>";
                            table += "<td class='text-center'>" + value.user + "</td>";
                            if(value.user_name !== null){
                                table += "<td class='text-center'>" + value.user_name + "</td>";
                            }else{
                                table += "<td class='text-center'></td>";
                            }
                            if(value.user_last_name !== null){
                                table += "<td class='text-center'>" + value.user_last_name + "</td>";
                            }else{
                                table += "<td class='text-center'></td>";
                            }
                            if(value.fullname !== null){
                                table += "<td class='text-center'>" + value.fullname + "</td>";
                            }else{
                                table += "<td class='text-center'></td>";
                            } 
                            if(value.user_role !== null){
                                table += "<td class='text-center'>" + value.user_role + "</td>";
                            }else{
                                table += "<td class='text-center'></td>";
                            }
                            if(value.position !== null){
                                table += "<td class='text-center'>" + value.position + "</td>";
                            }else{
                                table += "<td class='text-center'></td>";
                            }
                            if(value.activities !== null){
                                table += "<td class='text-center d-none'>" + value.activities + "</td>";
                            }else{
                                table += "<td class='text-center d-none'></td>";
                            }    
                            table += "<td class='text-center'>" + value.active + "</td>";
                            if(value.description !== null){
                                table += "<td class='text-center'>" + value.description + "</td>";
                            }else{
                                table += "<td class='text-center'></td>";
                            }
                            table += "<td class='text-center'>" + value.creation_date + "</td>";
                            table += "<td class='text-center'>";
                            table += "<a href='#!' class='link-update btn-link' title='Click to update' onclick='updateRecord(" + JSON.stringify(value) + ");'>";
                            table += "<img src='src/static/img/edit.png' class='img img-thumbnail' />";
                            table += "</a>";
                            table += "<td class='text-center'>";
                            table += "<a href='#!' class='link-update btn-link' title='Click to delete' onclick='deleteRecord(" + JSON.stringify(value) + ");'>";
                            table += "<img src='src/static/img/delete.png' class='img img-thumbnail' />";
                            table += "</a>";
                            table += "</td>";
                            table += "</tr>";
                        });

                        $('#tableData tbody').html(table);                        
                        $('#tableData').DataTable({
                            "scrollY": "450px",
                            "scrollCollapse": true,
//                            "paging": false,
//                            "info": false,
                            "retrieve": true,
                            language: {
                                "sProcessing":     "Processing...",
                                "sPaginationType": "full_numbers",
                                "sLengthMenu":     "Show _MENU_ records per page",
                                "sZeroRecords":    "No results found",
                                "sEmptyTable":     "No data available in this table",
                                "sInfo":           "Showing records from _START_ to _END_ of a total of _TOTAL_ records",
                                "sInfoEmpty":      "Showing records from 0 al 0 of a total of 0 records",
                                "sInfoFiltered":   "(filtering a total of _MAX_ records)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Search",
                                "sSearchPlaceholder": "Search",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Loading...",
                                "oPaginate": {
                                    "sFirst":    "First",
                                    "sLast":     "Last",
                                    "sNext":     "Next",
                                    "sPrevious": "Previous",
                                    "pagingType":"simple"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activate to sort the column in ascending order",
                                    "sSortDescending": ": Activate to sort the column in descending order"
                                }
                            }
                        });   
                    }
                }).done(function(){

                });
            }
            
            
            //            Delete record
            function deleteRecord(objFields)
            {
                $( "#dialog" ).html("Are you sure to delete record?");
                $( "#dialog" ).dialog({
                    title: "System information",
                    autoOpen: true,
                    modal: true,
                    buttons: [
                        {
                            text: "Ok",
                            click: function(){
                                $(this).dialog("close"); 

                                var objJson = {
                                    id: objFields.id_user,
                                    selectType: 'delete'
                                };

                                $.ajax({
                                    url: 'webservices/users-webservice.php',
                                    type: 'POST',
                                    dataType: 'json',
                                    data: JSON.stringify(objJson),
                                    success: function(data){
                                        $( "#dialog" ).html(data.message);
                                        $( "#dialog" ).dialog({
                                            autoOpen: true,
                                            modal: true,
                                            dialogClass: 'hide-close',      
                                            closeOnEscape: false,
                                            buttons: {
                                                "Ok": function () {
                                                    $(this).dialog("close");
                                                    location.reload();
                                                }
                                            } 
                                        });
                                    }
                                }).done(function(){
                                    
                                });
                            }
                        },
                        {
                            text: "Cancel",
                            click: function(){
                               $(this).dialog("close"); 
                            }
                        }
                    ] 
                });
            }
            
            
            //            Update record
            function updateRecord(objFields)
            {
                $( "#dialog" ).html("Are you sure to update record?");
                $( "#dialog" ).dialog({
                    title: "System information",
                    autoOpen: true,
                    modal: true,
                    dialogClass: 'hide-close',
                    closeOnEscape: false,
                    buttons: [
                        {
                            text: "Ok",
                            click: function(){
                                $(this).dialog("close"); 
                                $("#txtId").val(objFields.id_user);
                                $("#txtFirstName").val(objFields.user_name);
                                $("#txtLastName").val(objFields.user_last_name);
                                $("#txtRole").val(objFields.user_role);
                                $("#txtPosition").val(objFields.position);
                                $("#txtActivities").val(objFields.activities);
                                $("#txtUser").val(objFields.user);
                                $("#txtPassword").val(objFields.password);
                                $("#txtPasswordPrevious").val(objFields.password);
                                $("#cboProfiles").select2().val(objFields.profile_id);
                                $("#cboProfiles").select2().trigger('change');
                            }
                        },
                        {
                            text: "Cancel",
                            click: function(){
                                $(this).dialog("close"); 
                            }
                        }
                    ]
                });
            }
            
            
            function validataData()
            {
                $("#txtUser").val($("#txtUser").val().trim());
                $("#txtPassword").val($("#txtPassword").val().trim());
                $("#txtFirstName").val($("#txtFirstName").val().trim());
                $("#txtLastName").val($("#txtLastName").val().trim());
                var user = $("#txtUser").val();
                var password = $("#txtPassword").val();
                var firstName = $("#txtFirstName").val();
                var lastName = $("#txtLastName").val();
                var sw = false;

                if(user.length > 2 && user.length <= 255 && user.indexOf(" ", 0) === -1){
                    if(password.length > 3 && password.length <= 100){
                        if(firstName.length > 3 && firstName.length <= 255){
                            if(lastName.length > 3 && lastName.length <= 255){
                                sw = true;
                            }else{
                                showMessage('The last name field must contain at least 3 characters and a maximum of 255');
                            }    
                        }else{
                            showMessage('The first name field must contain at least 3 characters and a maximum of 255');
                        }
                    }else{
                        showMessage('The password field must contain at least 3 characters and a maximum of 100');
                    }
                }else{
                    showMessage('The user field must contain at least 3 characters and a maximum of 255 without white spaces');
                }
                return sw;
            }
            
        </script>
    </body>
</html>



