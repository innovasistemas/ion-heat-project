<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Bootstrap core CSS -->
        <link href="src/css/bootstrap.min.css" rel="stylesheet">
        <!-- jQuery-UI CSS -->
        <link href="src/css/jquery-ui-1.12-1.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="src/css/mdb.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="src/css/style.css">
        <!-- Your custom styles (optional) -->
        <link href="src/css/style.css" rel="stylesheet">
        <link rel="icon" href="src/static/img/cropped-Ion-Heat_Logo-2-32x32.png" sizes="32x32">
    </head>

    <body onload="loadData();">
        <div id="div-header" class="jumbotron blue-grey lighten-5" style="padding: 10px">
            <!--Header-->
            <div id="content-header"></div>
        </div>
        
        <!--Main container-->
        <div class="container-fluid">

            <!--Grid row-->
            <div class="row">

                <!--Grid column-->
                <div class="col-12">

                    <!--Card-->
                    
                    <div class="cardd border-warning">
                        <!--<h6 class="card-header text-white bg-primary h6">Login</h6>-->
<!--                        <div class="row">&nbsp;</div>
                        <div class="row">&nbsp;</div>
                        <div class="row">&nbsp;</div>-->
                        <!--Card content-->
                        <!--<div class="card-body text-center div-login">-->
                        <div class="row text-center">
                            <div class="col-1">&nbsp;</div>
                            <div class="col-10">
                            <!----------------------------------Your code Here---------------------------------------------->
                           
                                <form name="frmRegister" id="frmRegister">
<!--                                    <div class="row">
                                        <div class="col-12">
                                            <img src="src/static/img/logo_ionheat.png" />
                                        </div>
                                    </div>-->

                                    <div class="row">&nbsp;</div>    

                                    <div class="row">
                                        <div class="col-12">
                                            <h2>Log In</h2>
                                        </div>
                                    </div>

                                    <div class="row">&nbsp;</div>    
                                    <!--<div class="row">&nbsp;</div>-->

                                    <div class="row">
                                        <div class="col-1">&nbsp;</div>
                                        <div class="col-3 text-right">
                                            <label for="txtUserName" class="control-label">
                                                Username
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <input type="text" id="txtUserName" name="txtUserName" placeholder="Username" required="" class="form-control" autocomplete="off" autofocus="" />
                                        </div>
                                        <div class="col-1">&nbsp;</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-1">&nbsp;</div>
                                        <div class="col-3 text-right">
                                            <label for="txtPassword" class="control-label">
                                                Password
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <input type="password" id="txtPassword" name="txtPassword" placeholder="Password" required="" class="form-control" />
                                        </div>
                                        <div class="col-1">&nbsp;</div>
                                    </div>    
                                    <!--<div class="row">&nbsp;</div>-->
                                    <div class="row">&nbsp;</div>
                                    <div class="row">
                                        <div class="col-2">&nbsp;</div>
                                        <div class="col-9">
                                            <div class="form-group text-center text-bottom">
                                                <input type="button" value="Login" class="btn-primary text-capitalize w-75" id="btnSubmit" title="Login" />
                                                <!--<input type="reset" value="Cancel" class="btn btn-info btn-sm text-capitalize" id="btnReset" title="Reset" />-->
                                            </div>
                                        </div>
                                        <div class="col-1">&nbsp;</div>
                                    </div>
                                </form>
                            
                            <!---------------------------------- End Your code ---------------------------------------------->
                            </div>
                            <div class="col-1">&nbsp;</div>
                        </div>

                    </div>
                    <!--/.Card-->

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->

<!--User System Div-->

            <div id="dialog" title="System information"></div>
<!--User System Div-->


        </div>
        <!--Main container-->

        
        <!-- Scripts -->
        
        <!-- jQuery -->
        <script type="text/javascript" src="src/js/jquery-3.4.1.min.js"></script>
        <!-- jQuery-UI -->
        <script type="text/javascript" src="src/js/jquery-ui-1.12.1.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="src/js/mdb.min.js"></script>
        <!--Global function to encrypt-->
        <script type="text/javascript" src="src/js/aes3.1.2.js"></script>
        <!--Global functions Javascript-->
        <script type="text/javascript" src="src/js/main.js"></script>
        <script type="text/javascript" src="src/js/scripts-commons.js"></script>
        <script type="text/javascript" src="src/js/functions-commons.js"></script>
        
        <script>
            var saveInsertUpdate;

            $(function(){
                
                $("#btnSubmit").click(function(){
                    verifyCredentials(1);
                });


                $('#txtUserName').keypress(function(e){
                    if(e.which === 13){
                        verifyCredentials(1);
                    }
                });
                
                
                $('#txtPassword').keypress(function(e){
                    if(e.which === 13){
                        verifyCredentials(1);
                    }
                });
                
            });
            
            
            // Javascript functions 
            
            //-----------------------------------------------
            
            function loadData()
            {

            }
            
            
            function verifyCredentials(accessIndex)
            {
                var objJson = {
                    'username': $("#txtUserName").val(),
                    'password': $("#txtPassword").val(),
                    'accessIndex': accessIndex
                };

                $.ajax({
                    url: 'webservices/validate-credentials-webservice.php',
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(objJson),
                    success: function(data) {
                        if(data.error === 0){
                            var objJson = {
                                'profile': data.profile,
                                'accessIndex': 4
                            };
                            var userName = CryptoJS.AES.encrypt($("#txtUserName").val(), key);
                            var password = CryptoJS.AES.encrypt($("#txtPassword").val(), key);
                            var profile = CryptoJS.AES.encrypt(data.profile, key);
                            var access = CryptoJS.AES.encrypt(data.access.toString(), key);
                            var userKey = btoa("username");
                            var passKey = btoa("password");
                            var profileKey = btoa("profile");
                            var accessKey = btoa("access");
                            localStorage.setItem(userKey, userName);
                            localStorage.setItem(passKey, password);
                            localStorage.setItem(profileKey, profile);
                            localStorage.setItem(accessKey, access);
                            localStorage.setItem("full_name", data.full_name);
                            localStorage.setItem("token", data.token);
                            localStorage.setItem("ip_access", data.ip_access);
                            localStorage.setItem("user_agent", data.user_agent);
                            localStorage.setItem("sw", "true");
                            
                            $.ajax({
                                url: 'webservices/validate-credentials-webservice.php',
                                type: 'POST',
                                dataType: 'json',
                                data: JSON.stringify(objJson),
                                success: function(data) {
                                    if(data.error === 0){
                                        var menuId = btoa("id_component");
                                        var menuNames = btoa("menu");
                                        var menuLinks = btoa("links");
                                        var menuMainMenu = btoa("main_menu");
                                        var menuLogos = btoa("logos");
                                        var permissionsId = CryptoJS.AES.encrypt(data.idcomponent, key);
                                        var permissionsNames = CryptoJS.AES.encrypt(data.permissions, key);
                                        var permissionsLinks = CryptoJS.AES.encrypt(data.links, key);
                                        var permissionsMainMenu = CryptoJS.AES.encrypt(data.main_menu, key);
                                        var permissionLogos = CryptoJS.AES.encrypt(data.logos, key);
                                        localStorage.setItem(menuId, permissionsId);
                                        localStorage.setItem(menuNames, permissionsNames);
                                        localStorage.setItem(menuLinks, permissionsLinks);
                                        localStorage.setItem(menuMainMenu, permissionsMainMenu);
                                        localStorage.setItem(menuLogos, permissionLogos);
                                        $(location).attr('href', 'home.php');
                                    }else{
                                        localStorage.clear();
                                        showMessage(data.message);
                                    }
                                }
                            }).done(function(){
                                
                            });
                        }else{
                            showMessage(data.message);
                        }
                    }
                }).done(function(){

                }).fail(function(jqXHR, textStatus, errorThrown){
//                        https://cybmeta.com/manejo-de-errores-en-jquery-ajax
                    if (jqXHR.status === 0){
                        showMessage("You can't access the webservice at this moment because it isn't available or there isn't connection. Please try later or contact the administrator");
                    }else if(jqXHR.status === 404){
                        showMessage('Requested page not found [404]');
                    }else if(jqXHR.status === 500){
                        showMessage('Internal Server Error [500]');
                    }else if(textStatus === 'parsererror'){
                        showMessage('Requested JSON parse failed');
                    }else if(textStatus === 'timeout'){
                        showMessage('Time out error');
                    }else if(textStatus === 'abort'){
                        showMessage('Ajax request aborted');
                    }else{
                        showMessage('Uncaught Error: ' + jqXHR.responseText);
                    }
                });
                
            }
            
        </script>
    </body>
</html>












