<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="src/css/bootstrap.min.css" rel="stylesheet">
        <!-- jQuery-UI CSS -->
        <link href="src/css/jquery-ui-1.12-1.min.css" rel="stylesheet">
        <!-- jQuery datatable CSS -->
        <link href="src/css/jquery.dataTables.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="src/css/mdb.min.css" rel="stylesheet">
        <!--Select2 plugin-->
        <link href="src/css/select2.min.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="src/css/style.css" rel="stylesheet">
        <link rel="icon" href="src/static/img/cropped-Ion-Heat_Logo-2-32x32.png" sizes="32x32">
    </head>

    <body onload="verifyAccess(2); loadMainMenu(getPageName()); loadData();">
        
        <!--Menu-->
        <div id="div-header" class="navbar navbar-expand-lg navbar-light bg-light">
            <!--Header-->
                        
            <div id="content-header"></div>
            
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#div-menu" aria-controls="div-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="div-menu"></div>
            
            <!--User session-->
            <ul id="jsddm" class="text-capitalize" style="border-radius: 7px;">
                <li class="margin-div-user">
                    <img src='src/static/img/user-login.png' class="d-inline" />
                    <a href="#" id="link-login" class="text-center d-inline" style="border-radius: 7px;">Management</a>
                    <ul style="border-radius: 7px;">
                        <li>
                            <a href="#" id="link-profile">Profile</a>
                        </li>
                        <li>
                            <a href="#!" id="link-logout">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--end menu-->

        <!--Main container-->
        <div class="container-fluid">
            <!--Grid row-->
            <div class="row"></div>

            <!--Grid row-->
            <div class="row align-items-center">
                
                <!--Grid column-->
                <div class="col-lg-12 mb-4">
                    <!--Card-->
                    <div class="card">
                        <!--Card content-->
                        <div class="card-body">
                            <h2>Order cards home</h2>
                            <form>
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="txtUser" class="font-weight-bold">User</label>
                                        <input name="txtUser" id="txtUser" class="form-control input-lg w-50" disabled="">
                                    </div>
                                </div>
                                <div class="row">&nbsp;</div>
                                <div class="row">
                                    <div class="col-12">
                                        <label class="font-weight-bold">Components</label>
                                        <table id='tableData' class="table hover mdl-data-table">
                                            <!-- <caption><div id='resultTotal'>Recipes data</div></caption> -->
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Id</th>
                                                    <th class="text-center">Name</th>
                                                    <th class="text-center">Link</th>
                                                    <th class="text-center">Main menu</th>
                                                    <th class="text-center">
                                                        Order
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <input type="button" value="Save configuration" class="btn btn-success btn-md text-capitalize" id="btnSave" title="Save data" />
                                        <input type="reset" value="Cancel" class="btn btn-info btn-md text-capitalize" id="btnReset" title="Cancel" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--/.Card-->
                </div>
                <!--Grid column-->
                
            </div>
            <!--Grid row-->
            
            <div id="dialog" title="System information"></div>
            
        </div>
        <!--Main container-->
        

        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="src/js/jquery-3.4.1.min.js"></script>
        <!-- jQuery-UI -->
        <script type="text/javascript" src="src/js/jquery-ui-1.12.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="src/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
        <!--jQuery data tables-->
        <script type="text/javascript" src="src/js/jquery.dataTables.min.js"></script>
        <!--Bootstrap data tables-->
        <!--<script type="text/javascript" src="src/js/dataTables.bootstrap.min.js"></script>-->
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="src/js/mdb.min.js"></script>
        <script type="text/javascript" src="src/js/plotly.min.js"></script>
        <!--Select2 plugin-->
        <script type="text/javascript" src="src/js/select2.full.min.js"></script>
        <!--Global function to encrypt-->
        <script type="text/javascript" src="src/js/aes3.1.2.js"></script>
        <!--Global functions Javascript-->
        <script type="text/javascript" src="src/js/scripts-commons.js"></script>
        <script type="text/javascript" src="src/js/functions-commons.js"></script>

        <script>
//            var arrayComponents = [];
            
            $(function(){
                 setInterval(function(){
                    verifyAccess(2); 
                }, 5000);
                
                
                $("#btnSave").click(function(){
                    var arrayPermissionComponent = [];
                    var arrayNoPermissionComponent = [];
                    var objJson;
                    var i = 0;
                    var j = 0;
                    
                    $.each(arrayComponents, function(index, value){
                        if($("#chkComponent" + index).is(':checked')){
                            arrayPermissionComponent[i] = $("#chkComponent" + index).val();
                            i++;
                        }else{
                            arrayNoPermissionComponent[j] = $("#chkComponent" + index).val();
                            j++;
                        }
                    });
                    
                    objJson = {
                        profile: $("#cboProfiles").val(), 
                        permissionComponent: arrayPermissionComponent, 
                        noPermissionComponent: arrayNoPermissionComponent,
                        selectType: 'insert-delete'
                    };
                    
                    $.ajax({
                        url: 'webservices/permissions-webservice.php',
                        type: 'POST',
                        dataType: 'json',
                        data: JSON.stringify(objJson),
                        success: function(data){
                            location.reload();
                        }
                    }).done(function(){

                    });
                });
                
            });
            
            
            function loadData()
            {
                var userKey = localStorage.getItem(btoa("username"));
                var strUser = CryptoJS.AES.decrypt(userKey, key).toString(CryptoJS.enc.Utf8);
                
                $("#txtUser").val(strUser);
                
//                $( "#dialog" ).html("Do you want to create an order of cards per user?");
//                $( "#dialog" ).dialog({
//                    autoOpen: true,
//                    modal: true,
//                    dialogClass: 'hide-close',
//                    closeOnEscape: false,
//                    buttons: {
//                        "Ok": function(){
//                            $(this).dialog("close"); 
//                        },
//                        "Cancel": function(){
//                            $(this).dialog("close");
//                        }
//                    } 
//                });
                
                
                
                
                
                
                
                var fields = {
                    id: '',
                    name: '',
                    link: '',
                    order: '',
                    mainMenu: '',
                    selectType: 'read'
                };

                objJson = {
                    fields: fields
                };

                var strJson = JSON.stringify(objJson);
                var formData = new FormData();
                
                formData.append('dataSend', strJson);
                
                $.ajax({
                    url: 'webservices/components-webservice.php',
                    type: 'POST',
                    dataType: 'json',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(data){
                        var table = "";
                        $.each(data, function(index, value){
//                            arrayComponents[index] = value.id;
                            table += "<tr>";
                            table += "<td class='text-center'>" + value.id + "</td>";
                            table += "<td class='text-center'>" + value.name + "</td>";
                            table += "<td class='text-center'>" + value.link + "</td>";
                            table += "<td class='text-center'>" + value.main_menu + "</td>";
                            table += "<td class='text-center'>";
                            table += "<input type='number' class='form-control input-md w-25xxx' style='margin-left: 325px; width: 70px;' min='0' id='nbrOrder" + index + "' value='" + value.order + "'/>";                            table += "</td>";
                            table += "</tr>";
                        });

                        $('#tableData tbody').html(table);                        
                        $('#tableData').DataTable({
                            "scrollY": "350px",
                            "scrollCollapse": true,
                            "paging": false,
//                            "info": false,
                            "retrieve": true,
                            language: {
                                "sProcessing":     "Processing...",
                                "sPaginationType": "full_numbers",
                                "sLengthMenu":     "Show _MENU_ records per page",
                                "sZeroRecords":    "No results found",
                                "sEmptyTable":     "No data available in this table",
                                "sInfo":           "Showing records from _START_ to _END_ of a total of _TOTAL_ records",
                                "sInfoEmpty":      "Showing records from 0 al 0 of a total of 0 records",
                                "sInfoFiltered":   "(filtering a total of _MAX_ records)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Search",
                                "sSearchPlaceholder": "Search",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Loading...",
                                "oPaginate": {
                                    "sFirst":    "First",
                                    "sLast":     "Last",
                                    "sNext":     "Next",
                                    "sPrevious": "Previous",
                                    "pagingType":"simple"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activate to sort the column in ascending order",
                                    "sSortDescending": ": Activate to sort the column in descending order"
                                }
                            }
                        });   
                    }
                }).done(function(){

                });

                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                var fields = {
                    id: '',
                    name: '',
                    link: '',
                    order: '',
                    mainMenu: '',
                    selectType: 'read'
                };

                objJson = {
                    fields: fields
                };

                var strJson = JSON.stringify(objJson);
                var formData = new FormData();
                
                formData.append('dataSend', strJson);
                
                $.ajax({
                    url: 'webservices/components-webservice.php',
                    type: 'POST',
                    dataType: 'json',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(data){
                        var table = "";
                        $.each(data, function(index, value){
                            arrayComponents[index] = value.id;
                            table += "<tr>";
                            table += "<td class='text-center'>" + value.id + "</td>";
                            table += "<td class='text-center'>" + value.name + "</td>";
                            table += "<td class='text-center'>" + value.link + "</td>";
                            table += "<td class='text-center'>" + value.main_menu + "</td>";
                            table += "<td class='text-center'>";
                            table += "<input type='number' class='form-control input-md w-25xxx' style='margin-left: 325px; width: 70px;' min='0' id='nbrOrder" + index + "' value='" + value.order + "'/>";                            table += "</td>";
                            table += "</tr>";
                        });

                        $('#tableData tbody').html(table);                        
                        $('#tableData').DataTable({
                            "scrollY": "350px",
                            "scrollCollapse": true,
                            "paging": false,
//                            "info": false,
                            "retrieve": true,
                            language: {
                                "sProcessing":     "Processing...",
                                "sPaginationType": "full_numbers",
                                "sLengthMenu":     "Show _MENU_ records per page",
                                "sZeroRecords":    "No results found",
                                "sEmptyTable":     "No data available in this table",
                                "sInfo":           "Showing records from _START_ to _END_ of a total of _TOTAL_ records",
                                "sInfoEmpty":      "Showing records from 0 al 0 of a total of 0 records",
                                "sInfoFiltered":   "(filtering a total of _MAX_ records)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Search",
                                "sSearchPlaceholder": "Search",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Loading...",
                                "oPaginate": {
                                    "sFirst":    "First",
                                    "sLast":     "Last",
                                    "sNext":     "Next",
                                    "sPrevious": "Previous",
                                    "pagingType":"simple"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activate to sort the column in ascending order",
                                    "sSortDescending": ": Activate to sort the column in descending order"
                                }
                            }
                        });   
                    }
                }).done(function(){

                });
            }
            
        </script>
    </body>
</html>



