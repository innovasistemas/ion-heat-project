<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="src/css/bootstrap.min.css" rel="stylesheet">
        <!-- jQuery-UI CSS -->
        <link href="src/css/jquery-ui-1.12-1.min.css" rel="stylesheet">
        <!-- jQuery datatable CSS -->
        <link href="src/css/jquery.dataTables.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="src/css/mdb.min.css" rel="stylesheet">
        <!--Select2 plugin-->
        <link href="src/css/select2.min.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="src/css/style.css" rel="stylesheet">
        <link rel="icon" href="src/static/img/cropped-Ion-Heat_Logo-2-32x32.png" sizes="32x32">
    </head>

    <body onload="verifyAccess(5); loadMainMenu(getPageName()); loadData();">
        
        <!--Menu-->
        <div id="div-header" class="navbar navbar-expand-lg navbar-light bg-light">
            <!--Header-->
                        
            <div id="content-header"></div>
            
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#div-menu" aria-controls="div-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="div-menu"></div>
            
            <!--User session-->
            <ul id="jsddm" class="text-capitalize" style="border-radius: 7px;">
                <li class="margin-div-user">
                    <img src='src/static/img/user-login.png' class="d-inline" />
                    <a href="#" id="link-login" class="text-center d-inline" style="border-radius: 7px;">Management</a>
                    <ul style="border-radius: 7px;">
                        <li>
                            <a href="#" id="link-profile">Profile</a>
                        </li>
                        <li>
                            <a href="#!" id="link-logout">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--end menu-->

        <!--Main container-->
        <div class="container-fluid">
            <!--Grid row-->
            <div class="row"></div>

            <!--Grid row-->
            <div class="row align-items-center">
                
                <!--Grid column-->
                <div class="col-lg-12 mb-4">
                    <!--Card-->
                    <div class="card">
                        <!--Card content-->
                        <div class="card-body">
                            <h2>Access Log</h2>
                            <form>
                                <div class="row">
                                    <div class="col-12"> 
                                        <label for="cboUsers" class="font-weight-bold">User</label>
                                        <select name="cboUsers" id="cboUsers" class="form-control input-lg w-100 select2"></select>
                                    </div>
                                </div>
                                <div class="row">&nbsp;</div>
                                <!--<div class="card height-card">-->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <label class="font-weight-bold">Access</label>
                                            <table id='tableData' class="table hover mdl-data-table">
                                                <!-- <caption><div id='resultTotal'>Recipes data</div></caption> -->
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Id</th>
                                                        <th class="text-center">Token</th>
                                                        <th class="text-center">Ip access</th>
                                                        <th class="text-center">User agent</th>
                                                        <th class="text-center">User</th>
                                                        <th class="text-center">Creation date</th>
                                                        <th class="text-center">Exit date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--</div>-->
                                <div class="row">&nbsp;</div>
                                <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <label class="font-weight-bold">Operations</label>
                                        <table id='tableData2' class="table hover mdl-data-table">
                                            <!-- <caption><div id='resultTotal'>Recipes data</div></caption> -->
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Id</th>
                                                    <th class="text-center">Description</th>
                                                    <th class="text-center">Access Id</th>
                                                    <th class="text-center">Component</th>
                                                    <th class="text-center">Creation date</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--/.Card-->
                </div>
                <!--Grid column-->
                
            </div>
            <!--Grid row-->
            
            <div id="dialog" title="System information"></div>
            
        </div>
        <!--Main container-->
        

        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="src/js/jquery-3.4.1.min.js"></script>
        <!-- jQuery-UI -->
        <script type="text/javascript" src="src/js/jquery-ui-1.12.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="src/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
        <!--jQuery data tables-->
        <script type="text/javascript" src="src/js/jquery.dataTables.min.js"></script>
        <!--Bootstrap data tables-->
        <!--<script type="text/javascript" src="src/js/dataTables.bootstrap.min.js"></script>-->
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="src/js/mdb.min.js"></script>
        <script type="text/javascript" src="src/js/plotly.min.js"></script>
        <!--Select2 plugin-->
        <script type="text/javascript" src="src/js/select2.full.min.js"></script>
        <!--Global function to encrypt-->
        <script type="text/javascript" src="src/js/aes3.1.2.js"></script>
        <!--Global functions Javascript-->
        <script type="text/javascript" src="src/js/scripts-commons.js"></script>
        <script type="text/javascript" src="src/js/functions-commons.js"></script>

        <script>
            
            $(function(){
                 setInterval(function(){
                    verifyAccess(5); 
                }, 5000);
                
                
                $('#cboUsers').select2({
                    placeholder: 'Select an option',
                    language: "en",
                    allowClear: true,
                    tags: false,
                });
                
                
                $("#cboUsers").change(function(){
                    var objJson = {
                        id: $("#cboUsers").val(),
                        selectType: 'search'
                    };
                    
                    $.ajax({
                        url: 'webservices/access-webservice.php',
                        type: 'POST',
                        dataType: 'json',
                        data: JSON.stringify(objJson),
                        success: function(data){
                            var table = "";
                            $.each(data, function(index, value){
                                table += "<tr onclick='loadOperations(" + value.id + ")'>";
                                table += "<td class='text-center'>" + value.id + "</td>";
                                table += "<td class='text-center'>" + value.token + "</td>";
                                table += "<td class='text-center'>" + value.ip_access + "</td>";
                                table += "<td class='text-center'>" + value.user_agent + "</td>";
                                table += "<td class='text-center'>" + value.user + "</td>";
                                table += "<td class='text-center'>" + value.creation_date + "</td>";
                                table += "<td class='text-center'>" + value.exit_date + "</td>";
                                table += "</tr>";
                            });

                            $('#tableData tbody').html(table);                        
                            $('#tableData').DataTable({
                                "scrollY": "150px",
                                "scrollCollapse": true,
                                "paging": false,
                                "info": false,
                                "retrieve": true,
                                "order": [[0, "desc"]],
                                language: {
                                    "sProcessing":     "Processing...",
                                    "sPaginationType": "full_numbers",
                                    "sLengthMenu":     "Show _MENU_ records per page",
                                    "sZeroRecords":    "No results found",
                                    "sEmptyTable":     "No data available in this table",
                                    "sInfo":           "Showing records from _START_ to _END_ of a total of _TOTAL_ records",
                                    "sInfoEmpty":      "Showing records from 0 al 0 of a total of 0 records",
                                    "sInfoFiltered":   "(filtering a total of _MAX_ records)",
                                    "sInfoPostFix":    "",
                                    "sSearch":         "Search",
                                    "sSearchPlaceholder": "Search",
                                    "sUrl":            "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Loading...",
                                    "oPaginate": {
                                        "sFirst":    "First",
                                        "sLast":     "Last",
                                        "sNext":     "Next",
                                        "sPrevious": "Previous",
                                        "pagingType":"simple"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activate to sort the column in ascending order",
                                        "sSortDescending": ": Activate to sort the column in descending order"
                                    }
                                }
                            });   
                        }
                    }).done(function(){

                    });
                });
                
            });
            
            
            function loadData()
            {
                var objJson = {
                    selectType: 'read'
                };
                
                $.ajax({
                    url: 'webservices/users-webservice.php',
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(objJson),
                    success: function(data){
                        var option = "<option>Select an option</option>";
                        $.each(data, function(index, value){
                            option += "<option value='" + value.id_user + "'>" + value.user_name + " - " + value.user_last_name + "</option>";
                        });
                        $("#cboUsers").html(option);
                    }
                }).done(function(){
                    
                });
            }
            
            
            function loadOperations(id)
            {
                var objJson = {
                    id: id,
                    selectType: 'search'
                };

                $.ajax({
                    url: 'webservices/operations-webservice.php',
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(objJson),
                    success: function(data){
                        var table = "";
                        if(data.length > 0){
                            $.each(data, function(index, value){
                                table += "<tr>";
                                table += "<td class='text-center'>" + value.id + "</td>";
                                table += "<td class='text-center'>" + value.description + "</td>";
                                table += "<td class='text-center'>" + value.access_id + "</td>";
                                table += "<td class='text-center'>" + value.name + "</td>";
                                table += "<td class='text-center'>" + value.creation_date + "</td>";
                                table += "</tr>";
                            });
                        }

                        $('#tableData2 tbody').html(table);                        
                        $('#tableData2').DataTable({
                            "scrollY": "150px",
                            "scrollCollapse": true,
                            "paging": false,
                            "info": false,
                            "retrieve": true,
                            language: {
                                "sProcessing":     "Processing...",
                                "sPaginationType": "full_numbers",
                                "sLengthMenu":     "Show _MENU_ records per page",
                                "sZeroRecords":    "No results found",
                                "sEmptyTable":     "No data available in this table",
                                "sInfo":           "Showing records from _START_ to _END_ of a total of _TOTAL_ records",
                                "sInfoEmpty":      "Showing records from 0 al 0 of a total of 0 records",
                                "sInfoFiltered":   "(filtering a total of _MAX_ records)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Search",
                                "sSearchPlaceholder": "Search",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Loading...",
                                "oPaginate": {
                                    "sFirst":    "First",
                                    "sLast":     "Last",
                                    "sNext":     "Next",
                                    "sPrevious": "Previous",
                                    "pagingType":"simple"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activate to sort the column in ascending order",
                                    "sSortDescending": ": Activate to sort the column in descending order"
                                }
                            }
                        });   
                    }
                }).done(function(){

                });
            }
            
            
            function selectCheckbox()
            {
                $.each(arrayComponents, function(index, value){
                    $("#chkComponent" + index).attr('checked', 'checked');
                });
            }
            
            
            function deselectCheckbox()
            {
                $.each(arrayComponents, function(index, value){
                    $("#chkComponent" + index).removeAttr('checked');
                });
            }
            
        </script>
    </body>
</html>



