<!DOCTYPE html>
<html>
    <head>
        <title>IONHEAT DMI TEST</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="src/css/bootstrap.min.css" rel="stylesheet">
        <!-- jQuery-UI CSS -->
        <link href="src/css/jquery-ui-1.12-1.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="src/css/mdb.min.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="src/css/style.css" rel="stylesheet">
        <link rel="icon" href="src/static/img/cropped-Ion-Heat_Logo-2-32x32.png" sizes="32x32">
    </head>

    <body onload="verifyAccess(2);">
        <div id="div-header" class="jumbotron blue-grey lighten-5" style="padding: 12px">
            <!--Header-->
            <!--<div id="content-header"></div>-->
<!--            <table styles="margin-top: 50px;">
            <tr>-->
                <!--<td class="text-left">-->
                    <a href="#!">
                        <img src="src/static/img/logo_ionheat.png" class="link-home" alt="logo IonHeat" />
                    </a>
<!--                </td>
            </tr>
        </table>-->
            
            <!--Menú-->
            <div class="d-inline" id="div-menu"></div>
            
            <!--User session-->
            <ul id="jsddm" class="text-capitalize" style="border-radius: 7px;">
                <li class="margin-div-user" style="margin-top: -40px;">
                    <img src='src/static/img/user-login.png' class="d-inline" />&nbsp;&nbsp;&nbsp;
                    <a href="#" id="link-login" class="text-center d-inline" style="border-radius: 7px;">Management</a>
                    <ul style="border-radius: 7px;">
                        <li>
                            <a href="#" id="link-profile">Profile</a>
                        </li>
                        <li>
                            <a href="#!" id="link-logout">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

        <!--Main container-->
        <div class="container-fluid">
            <!--Grid row-->
            <div class="row align-items-center" id="div-card-menu"></div>
            
            <div id="dialog" title="System information"></div>
            
        </div>
        <!--Main container-->

        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="src/js/jquery-3.4.1.min.js"></script>
        <!-- jQuery-UI -->
        <script type="text/javascript" src="src/js/jquery-ui-1.12.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="src/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
        <!--jQuery data tables-->
        <script type="text/javascript" src="src/js/jquery.dataTables.min.js"></script>
        <!--Bootstrap data tables-->
        <script type="text/javascript" src="src/js/dataTables.bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="src/js/mdb.min.js"></script>
        <script type="text/javascript" src="src/js/plotly.min.js"></script>
        <!--Global function to encrypt-->
        <script type="text/javascript" src="src/js/aes3.1.2.js"></script>
        <!--Global functions Javascript-->
        <script type="text/javascript" src="src/js/scripts-commons.js"></script>
        <script type="text/javascript" src="src/js/functions-commons.js"></script>

        <script>

            $(function(){
                setInterval(function(){
                    verifyAccess(2); 
                }, 5000);
                
            });
            
        </script>
    </body>
</html>



