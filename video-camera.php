<!DOCTYPE html>
<html>
    <head>
        <title>IONHEAT DMI TEST</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="src/css/bootstrap.min.css" rel="stylesheet">
        <!-- jQuery-UI CSS -->
        <link href="src/css/jquery-ui-1.12-1.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="src/css/mdb.min.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="src/css/style.css" rel="stylesheet">
        <link rel="icon" href="src/static/img/cropped-Ion-Heat_Logo-2-32x32.png" sizes="32x32">
    </head>

    <body onload="//verifyAccess(2);">
        <div id="div-header" class="jumbotron blue-grey lighten-5" style="padding: 12px">
            <!--Header-->
            <!--<div id="content-header"></div>-->
<!--            <table styles="margin-top: 50px;">
            <tr>-->
                <!--<td class="text-left">-->
                    <a href="#!">
                        <img src="src/static/img/logo_ionheat.png" class="link-home" alt="logo IonHeat" />
                    </a>
<!--                </td>
            </tr>
        </table>-->
            
            <!--Menú-->
            <div class="d-inline" id="div-menu"></div>
            
            <!--User session-->
            <ul id="jsddm" class="text-capitalize" style="border-radius: 7px;">
                <li class="margin-div-user" style="margin-top: -40px;">
                    <img src='src/static/img/user-login.png' class="d-inline" />&nbsp;&nbsp;&nbsp;
                    <a href="#" id="link-login" class="text-center d-inline" style="border-radius: 7px;">Management</a>
                    <ul style="border-radius: 7px;">
                        <li>
                            <a href="#" id="link-profile">Profile</a>
                        </li>
                        <li>
                            <a href="#!" id="link-logout">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

        <!--Main container-->
        <div class="container-fluid">
            <!--Grid row-->
            <div class="row align-items-center" id="div-card-menu"></div>
            
            <h3>P1</h3>
            <iframe src="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/httppreview" id="frame-video" name="frame-video"></iframe>
            <iframe src="http://172.16.1.29:554/Streaming/Channels/101/httppreview" id="frame-video2" name="frame-video2"></iframe>
            <iframe src="http://172.16.1.29:554/" id="frame-video3" name="frame-video3"></iframe>
            
            <h3>P2</h3>
            <embed type="application/x-vlc-plugin" pluginspage="http://www.videolan.org" autoplay="yes" loop="no" width="300" height="200" target="rtsp://admin:ionmheat2018*@172.16.1.29:554/Streaming/Channels/102/" />
            
            <h3>P3</h3>
            <object classid="clsid:9BE31822-FDAD-461B-AD51-BE1D1C159921" codebase="http://download.videolan.org/pub/videolan/vlc/last/win32/axvlc.cab" style="display:none;"></object>
            
            <h3>P4</h3>
            <OBJECT classid="clsid:9BE31822-FDAD-461B-AD51-BE1D1C159921"
                    codebase="http://downloads.videolan.org/pub/videolan/vlc/latest/win32/axvlc.cab"
                    accesskey=""width="640" height="480" id="vlc" events="True">
                <param name="Src" value="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/" />
                <!--<param name="Src" value="rtsp://admin:ionmheat2018*@172.16.1.29:554/Streaming/Channels/102/" />-->
                <param name="ShowDisplay" value="True" />
                <param name="AutoLoop" value="False" />
                <param name="AutoPlay" value="True" />
<!--                <embed id="vlcEmb"  type="application/x-google-vlc-plugin" version="VideoLAN.VLCPlugin.2" autoplay="yes" loop="no" width="640" height="480"
                    target="rtsp://admin:ionmheat2018*@172.16.1.29:554/Streaming/Channels/102" ></embed>-->
                <embed id="vlcEmb"  type="application/x-google-vlc-plugin" version="VideoLAN.VLCPlugin.2" autoplay="yes" loop="no" width="640" height="480"
                    target="frame-video" ></embed>
            </OBJECT>
            
            <h3>P5</h3>
            <div id="the_div_that_will_hold_the_player_object"></div>
                        
            <h3>P6</h3>
            <object ID="MediaPlayer" WIDTH="320" HEIGHT="270" CLASSID="CLSID:22D6f312-B0F6-11D0-94AB-0080C74C7E95" STANDBY="Loading Windows Media Player components..." TYPE="application/x-oleobject" CODEBASE="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,7,1112">

                <param name="autoStart" value="True">

                <!--<param name="filename" value="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/">-->
                <param name="filename" value="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/">

                <param NAME="ShowControls" VALUE="False">

                <param NAME="ShowStatusBar" VALUE="False">

                <embed TYPE="application/x-mplayer2" SRC="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/" NAME="MediaPlayer" WIDTH="320" HEIGHT="270" autostart="1" showcontrols="0"></embed>
                <!--<embed TYPE="application/x-mplayer2" SRC="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/" NAME="MediaPlayer" WIDTH="320" HEIGHT="270" autostart="1" showcontrols="0"></embed>-->
            </object>
<!-- webbot bot="HTMLMarkup" endspan ---->

<!--- end PLAYER --->
            
            <h3>P7</h3>
            <video id="videoElement"></video>

            
            <h3>P8</h3>
            <video id="videoElement">
                <source src="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/" type="application/x-rtsp">
            </video>
            
            
            <h3>P9</h3>
            <embed id="vlc" type="application/x-vlc-plugin" version="VideoLAN.VLCPlugin.2" width="640" height="480"></embed>

            <h3>P10</h3>
            <session>
                <group language="en lipsync">
                    <switch>
                    <track type=audio e="PCMU/8000/1" src="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/">
                    <track type=audio e="DVI4/16000/2" pt="90 DVI4/8000/1" src="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/">
                    </switch>
                    <track type="video/jpeg" src="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/">
                </group>
            </session>
            
            <h3>P11</h3>
            <!--<iframe src="http://201.144.111.148/web/generar.php?nombre=DS-9116HDI-S1620100813BBRR401315773WCVU&usuario=web&password=12345&puerto=8000&camara=10&ptz=1" width="360" height=280" frameborder="0" scrolling="no">-->
            <!--<iframe src="http://172.16.1.29:554?nombre=DS-9116HDI-S1620100813BBRR401315773WCVU&usuario=admin&password=ionheat2018*&puerto=8000&camara=10&ptz=1" width="360" height=280" frameborder="0" scrolling="no">-->
            <iframe src="http://172.16.1.29:554?nombre=DS-9116HDI-S1620100813BBRR401315773WCVU&usuario=admin&password=ionheat2018*&puerto=8000&camara=1&ptz=1" width="360" height=280" frameborder="0" scrolling="no">
                <p>Su navegador no soporta iFrames</p>
            </iframe>
            
            <h3>P12</h3>
            <a href="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/" target="_blank" onclick="window.open(this.href, this.target, 'width=900,height=900,top=200px,left=300px'); return false;">Camera</a>
            
            <h3>P13</h3>
            <!--<img src="http://admin:ionheat2018*@172.16.1.24/ISAPI/Streaming/channels/102/picture?videocodec=jpeg" width="100%" />-->
            <!--<img src="http://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/picture?videocodec=jpeg" width="100%" />-->
            <img src="http://admin:ionheat2018*@172.16.1.29/ISAPI/Streaming/channels/101/picture?videocodec=jpeg"/>
            
            <h3>P14</h3>
            <div>
                <p>videos VLC</p>
                <label for="s2id_autogen2" class="select2-offscreen"></label>
                <embed type="application/x-vlc-plugin" name="VLC" autoplay="yes" loop="no" volume="100" width="640" height="480" target="" controls>
            </div>
            
            <h3>P15</h3>
            <embed type="application/x-vlc-plugin" pluginspage="http://www.videolan.org" version="VideoLAN.VLCPlugin.2" width="640" height="480" target="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/" id="vlc"> 
            
            <h3>P16</h3>
            <!--<div id="vxg_media_player1" class="vxgplayer" width="300" height="240" url="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/" aspect-ratio latency="3000000" autostart controls avsync debug></div>-->

            
<!--            <video src="rtp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/">
                Your browser does not support the VIDEO tag and/or RTP streams.
            </video>-->
            
            <video id="test_video" controls autoplay width="400" height="300">
                <!--<source src="natively_supported_video_url" data-ignore="true">-->
                <source src="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/">
            </video>
            
            <h3>P17</h3>
            <video id="test_video2" controls>
                <!--<source src="natively_supported_video_url" data-ignore="true">-->
                <source src="rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/101/" type="video/mp4">
                Your browser don't support video HTML5
            </video>
            
            
            <div id="dialog" title="System information"></div>
            
        </div>
        <!--Main container-->

        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="src/js/jquery-3.4.1.min.js"></script>
        <!-- jQuery-UI -->
        <script type="text/javascript" src="src/js/jquery-ui-1.12.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="src/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
        <!--jQuery data tables-->
        <script type="text/javascript" src="src/js/jquery.dataTables.min.js"></script>
        <!--Bootstrap data tables-->
        <script type="text/javascript" src="src/js/dataTables.bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="src/js/mdb.min.js"></script>
        <script type="text/javascript" src="src/js/plotly.min.js"></script>
        <!--Global function to encrypt-->
        <script type="text/javascript" src="src/js/aes3.1.2.js"></script>
        <!--Global functions Javascript-->
        <script type="text/javascript" src="src/js/scripts-commons.js"></script>
        <script type="text/javascript" src="src/js/functions-commons.js"></script>
        
<!--        <script src="https://cdn.bootcss.com/flv.js/1.5.0/flv.min.js"></script>-->
        
        <script>
//            if (flvjs.isSupported()) {
//                var videoElement = document.getElementById('videoElement');
//                var flvPlayer = flvjs.createPlayer({
//                    type: 'flv',
////                        url: 'ws://localhost:8000/live/STREAM_NAME.flv'
////                    url: 'rtsp://admin:ionmheat2018*@172.16.1.29:554/Streaming/Channels/102/'
//                    url: 'rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/'
//                });
//                flvPlayer.attachMediaElement(videoElement);
//                flvPlayer.load();
//                flvPlayer.play();
//            }
        </script>
        
        <script>
//            var session = Flashphoner.createSession({urlServer:"wss://172.16.1.29:554"});
//            session.createStream({name:"rtsp://admin:ionheat2018*@172.16.1.29:554/", display:myVideo}).play();
//            var vlc = document.getElementById('vlc');
//            vlc .playlist.add('rtsp://admin:ionmheat2018*@172.16.1.29:554/Streaming/Channels/102/');
//            vlc.playlist.play();
        </script>

        <script>

            $(function(){
//                setInterval(function(){
//                    verifyAccess(2); 
//                }, 5000);

                //SET THE RTSP STREAM ADDRESS HERE
//                var address = "rtsp://192.168.0.102/mpeg4/1/media.3gp";
//                var address = "rtsp://admin:ionheat2018*@172.16.1.29:554/Streaming/Channels/102/";
//                var address = "rtsp://admin:ionmheat2018*@172.16.1.29:554/Streaming/Channels/102/";

//                var output = '<object width="640" height="480" id="qt" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab">';
//                    output += "<param name='src' value='" + address + "'>";
//                    output += '<param name="autoplay" value="true">';
//                    output += '<param name="controller" value="false">';
//                    output += '<embed id="plejer" name="plejer" src="rtsp://admin:ionmheat2018*@172.16.1.29:554/Streaming/Channels/102/" bgcolor="000000" width="640" height="480" scale="ASPECT" qtsrc="' + address + '" kioskmode="true" showlogo=false" autoplay="true" controller="false" pluginspage="http://www.apple.com/quicktime/download/">';
//                    output += '</embed></object>';

                //SET THE DIV'S ID HERE
//                document.getElementById("the_div_that_will_hold_the_player_object").innerHTML = output;
                
            });
            
            
            
            
        </script>
    </body>
</html>



